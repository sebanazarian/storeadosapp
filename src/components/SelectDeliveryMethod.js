import React, { useEffect, useState } from 'react';

export const SelectDeliveryMethod = ({ deliveryMethods = [], errors, onChange }) => {

    const [value, setValue] = useState();

    useEffect(() => {
        onChange && onChange(value)
    }, [value])

    return (
        <div className="App-DatosPago-Forma">
            {
                deliveryMethods.map((delivery, index) =>
                    <div key={delivery.id}
                        className={`BoxFormaPago BoxTipoEnvio BorderAll BgDegradeRosaV ${value == delivery.id ? 'Active' : ''}`}
                        onClick={() => setValue(delivery.id)}
                    >
                        <i className="IconChek fa fa-check-circle-o"></i>
                        <i className={`Icon IconTarjeta fa fa-${delivery.icon}`}></i>
                        <p className="Title">{delivery.name}</p>
                    </div>
                )
            }
            {
                errors && errors.map((message, index) => (
                    <div className="invalid-feedback" key={index}>
                        {message}
                    </div>
                ))
            }
        </div>

    )
}
