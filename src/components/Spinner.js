import React from 'react';
import { IonSpinner } from '@ionic/react'

export const Spinner = () => {
    return (
        <IonSpinner style={{ 'margin': '0 50%' }} name="crescent" />
    )
}
