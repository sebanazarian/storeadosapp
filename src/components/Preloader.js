import React, { useEffect, useRef } from 'react';

import loading from "assets/img/precarga/loading.gif";

export const Preloader = () => {
    const $ = window.jQuery

    const refPreloader = useRef()
    const refStatus = useRef()

    useEffect(() => {
        $(window).bind("load", function () {
            $(refPreloader.current).fadeOut();
            $(refStatus.current).delay(450).fadeOut("slow");
        })

        return () => $(window).unbind("load")
    }, [])

    return (
        <div id="preloader" ref={refPreloader}>
            <div id="status" ref={refStatus}>
                <img src={loading} />
            </div>
        </div>
    )
}
