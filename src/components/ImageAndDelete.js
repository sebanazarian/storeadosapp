import React from 'react';

import { IonImg } from '@ionic/react';
import { Button } from 'components';

import config from 'config';

export const ImageAndDelete = ({ type = 'medium', path, src, size = '200', onError, onDidLoad, onDelete, textDelete = 'Eliminar' }) => {
   
    let url

    if (!src) {
        url = `${config.url}/images/${type}/${path}`

        if (path && path.includes("http"))
            url = path;
    }

    return (
        <>
            <IonImg src={url || src}
                style={{
                    height: size + 'px',
                    objectFit: 'cover',
                    borderTopRightRadius: '7px',
                    borderTopLeftRadius: '7px',
                    overflow: 'hidden'
                }}
                onIonError={(event) => onError && onError(event)}
                onIonImgDidLoad={(event) => onDidLoad && onDidLoad(event)}
            />
            <Button onClick={(event) => onDelete && onDelete(event)} className="BorderBottom">{textDelete}</Button>
        </>
    )
}
