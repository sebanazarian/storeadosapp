import React from 'react';
import { IonContent } from '@ionic/react';

export const Content = ({ children, margin, className}) => {
    return (
        <section className={`App-Content BgWhite ${className} ${margin ? 'App-Titlebar' : ''}`}>
            <div className="container">
                {children}
            </div>
        </section>
    )
}
