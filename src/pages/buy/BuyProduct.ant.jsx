import React, { useState, useEffect } from 'react'
import { useHistory, useParams } from "react-router-dom"
import {
    IonContent,
    IonPage,
    IonTitle,
    IonToolbar,
    IonButton,
    IonItem,
    IonInput,
    IonLabel,
    IonImg
} from '@ionic/react'

import { ProductService } from 'services'

import { Title } from 'components'

import config from 'config'

export const BuyProduct = () => {

    const history = useHistory()
    const params = useParams()

    const [product, setProduct] = useState()
    const [quantity, setQuantity] = useState(1)

    const loadProduct = async () => {
        try {
            let product = await ProductService.detail(params.id)
            setProduct(product)
        } catch (error) {
            console.error(error)
        }
    }

    useEffect(() => {
        setProduct(history.location?.state?.product)
        loadProduct()
    }, [])

    return (
        <IonPage>
            <Title>{product?.name || ''}</Title>
            <IonToolbar>
                <IonTitle>{product?.name || ''}</IonTitle>
            </IonToolbar>
            {
                product &&
                <IonContent>
                    {product.gallery.map((photo, index) => <IonImg key={index} src={`${config.url}/images/small/${photo}`} />)}
                    <IonItem>
                        <IonLabel>{product.category.name}</IonLabel>
                    </IonItem>
                    <IonItem>
                        <IonLabel>$ {Intl.NumberFormat('es', { minimumFractionDigits: 2 }).format(product.price)}</IonLabel>
                    </IonItem>
                    <IonItem>
                        <IonLabel>Cantidad</IonLabel>
                        <IonInput
                            type="number"
                            value={quantity}
                            placeholder="Cantidad"
                            onIonChange={e => setQuantity(e.detail.value)}
                            required />
                    </IonItem>
                    <p>
                        {product.detail}
                    </p>
                    <IonButton
                        expand="block"
                        onClick={() => history.push(`/${params.id}-${params.slug}/buy`, { product, quantity })}
                        color="success"
                        disabled={!quantity}
                    >
                        Comprar
                    </IonButton>
                </IonContent>
            }
        </IonPage>
    )
}