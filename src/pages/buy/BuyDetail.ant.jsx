import React, { useState, useEffect } from 'react'
import { useHistory, useParams } from "react-router-dom"
import {
    IonContent,
    IonPage,
    IonTitle,
    IonToolbar,
    IonBackButton,
    IonButton,
    IonButtons,
    IonItem,
    IonInput,
    IonIcon,
    IonText,
    IonLoading,
    IonAlert,
    IonLabel,
    IonList,
    IonRadioGroup,
    IonListHeader,
    IonRadio,
    IonSpinner,
} from '@ionic/react'

import { closeCircleOutline } from 'ionicons/icons'

import {
    ProductService,
    PaymentMethodService,
    PurchaseOrderService
} from 'services'

import { Title } from 'components'

export const BuyDetail = () => {

    const history = useHistory()
    const params = useParams()

    const [errors, setErrors] = useState({})
    const [showAlertError, setShowAlertError] = useState(false)
    const [alertMessage, setAlertMessage] = useState()
    const [submitting, setSubmitting] = useState(false)

    const [paymentMethods, setPaymentMethods] = useState([])

    const [product, setProduct] = useState()
    const [paymentMethod, setPaymentMethod] = useState(1)
    const [deliveryMethod, setDeliveryMethod] = useState(1)
    const [quantity, setQuantity] = useState(1)
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [email, setEmail] = useState('')
    const [phone, setPhone] = useState('')
    const [streetName, setStreetName] = useState('')
    const [streetNameAlt, setStreetNameAlt] = useState()
    const [betweenStreets, setBetweenStreets] = useState()
    const [streetNumber, setStreetNumber] = useState(3040)
    const [zipCode, setZipCode] = useState(3000)
    const [countryName, setCountryName] = useState('')
    const [stateName, setStateName] = useState('')
    const [cityName, setCityName] = useState('')
    const [reference, setReference] = useState()

    const [amount, setAmount] = useState(0)

    const load = async () => {
        try {
            let [
                product,
                paymentMethods
            ] = await Promise.all([
                ProductService.detail(params.id),
                PaymentMethodService.all()
            ])


            setProduct(product)
            setPaymentMethods(paymentMethods)
        } catch (error) {
            console.error(error)
        }
    }

    useEffect(() => {
        setProduct(history.location?.state?.product)
        setQuantity(history.location?.state?.quantity || 1)
        load()
    }, [])

    useEffect(() => {
        if (!product) return
        setAmount(quantity * product.price)

        if (paymentMethod == 3) {
            setDeliveryMethod(null)
        }
    }, [product, deliveryMethod, paymentMethod, quantity])

    const estimatePurchaseOrder = async () => {
        setSubmitting(true)
        try {
            let data = {
                payment_method: paymentMethod,
                delivery_method: deliveryMethod,
                first_name: firstName,
                last_name: lastName,
                email,
                phone,
                street_name: streetName,
                street_name_alt: streetNameAlt,
                between_streets: betweenStreets,
                street_number: streetNumber,
                zip_code: zipCode,
                country_name: countryName,
                state_name: stateName,
                city_name: cityName,
                reference,
                products: [
                    {
                        product_id: product.id,
                        quantity
                    }
                ]
            }

            let purchaseOrder = await PurchaseOrderService.estimate(data)

            console.log(purchaseOrder)

            history.push(`/${params.id}-${params.slug}/resume`, {
                product,
                purchaseOrder,
                createPurchaseOrder: data,
            })
        } catch (error) {
            console.error(error)
            if (error?.response?.data?.errors) {
                setErrors(error?.response?.data?.errors)
            } else {
                setAlertMessage(error?.response?.data?.message)
                setShowAlertError(true)
            }
        }
        setSubmitting(false)
    }

    const renderErrorMessage = (field) => {
        return errors[field] && errors[field].map((message, index) => (
            <IonText key={field + index} color="danger">
                <p style={{ display: 'flex', alignItems: 'center' }}>
                    <IonIcon icon={closeCircleOutline} style={{ fontSize: '25px', marginLeft: '20px', marginRight: '10px' }} />
                    {message}
                </p>
            </IonText>
        ))
    }

    const renderErrorMessageDefault = (field, message) => {
        return errors[field] && (
            <IonText color="danger">
                <p style={{ display: 'flex', alignItems: 'center' }}>
                    <IonIcon icon={closeCircleOutline} style={{ fontSize: '25px', marginLeft: '20px', marginRight: '10px' }} />
                    {message}
                </p>
            </IonText>
        )
    }

    return (
        <IonPage>
            <Title>Comprar {product?.name || ''}</Title>
            <IonToolbar>
                <IonButtons slot="start">
                    <IonBackButton defaultHref={`/${params.id}-${params.slug}`} />
                </IonButtons>
                <IonTitle>Comprar {product?.name || ''}</IonTitle>
            </IonToolbar>
            <IonContent>
                <IonList>
                    <IonRadioGroup value={paymentMethod} onIonChange={e => setPaymentMethod(e.detail.value)}>
                        <IonListHeader>
                            <IonLabel>Forma de Pago</IonLabel>
                        </IonListHeader>
                        {
                            !paymentMethods.length
                                ? <IonSpinner name="crescent" />
                                : paymentMethods.map(payment =>
                                    <IonItem key={payment.id} disabled={
                                        !((!payment.min_allowed_amount || payment.min_allowed_amount < amount) &&
                                            (!payment.max_allowed_amount || payment.max_allowed_amount >= amount))}>
                                        <IonLabel>{payment.name}</IonLabel>
                                        <IonRadio slot="start" value={payment.id} />
                                    </IonItem>
                                )
                        }
                    </IonRadioGroup>
                </IonList>
                {renderErrorMessageDefault('payment_method', 'La forma de pago es obligatorio')}
                {
                    product && <IonList>
                        <IonRadioGroup value={deliveryMethod} onIonChange={e => setDeliveryMethod(e.detail.value)}>
                            <IonListHeader>
                                <IonLabel>Forma de Entrega</IonLabel>
                            </IonListHeader>
                            {
                                product.delivery_methods.map(delivery =>
                                    <IonItem key={delivery.id} disabled={paymentMethod == 3}>
                                        <IonLabel>{delivery.name}</IonLabel>
                                        <IonRadio slot="start" value={delivery.id} />
                                    </IonItem>
                                )
                            }
                        </IonRadioGroup>
                    </IonList>
                }
                <IonItem>
                    <IonLabel><strong>Datos del Comprador</strong></IonLabel>
                </IonItem>
                <IonItem>
                    <IonLabel>Cantidad</IonLabel>
                    <IonInput
                        type="number"
                        value={quantity}
                        placeholder="Cantidad"
                        onIonChange={e => setQuantity(e.detail.value)}
                        required />
                </IonItem>
                {renderErrorMessage('products.0.quantity')}
                <IonItem>
                    <IonLabel>Total $ {Intl.NumberFormat('es', { minimumFractionDigits: 2 }).format(amount)}</IonLabel>
                </IonItem>
                <IonItem>
                    <IonLabel><strong>Datos del Comprador</strong></IonLabel>
                </IonItem>
                <IonItem>
                    <IonInput
                        type="text"
                        value={firstName}
                        placeholder="Nombre"
                        onIonChange={e => setFirstName(e.detail.value)}
                        disabled={submitting}
                        required />
                </IonItem>
                {renderErrorMessage('first_name')}
                <IonItem>
                    <IonInput
                        type="text"
                        value={lastName}
                        placeholder="Apellido"
                        onIonChange={e => setLastName(e.detail.value)}
                        disabled={submitting}
                        required />
                </IonItem>
                {renderErrorMessage('last_name')}
                <IonItem>
                    <IonInput
                        type="text"
                        value={email}
                        placeholder="Email"
                        onIonChange={e => setEmail(e.detail.value)}
                        disabled={submitting}
                        required />
                </IonItem>
                {renderErrorMessage('email')}
                <IonItem>
                    <IonInput
                        type="text"
                        value={phone}
                        placeholder="Telefono"
                        onIonChange={e => setPhone(e.detail.value)}
                        disabled={submitting}
                        required />
                </IonItem>
                {renderErrorMessage('phone')}
                <IonItem>
                    <IonLabel><strong>Datos del Domicilio de Entrega</strong></IonLabel>
                </IonItem>
                <IonItem>
                    <IonInput
                        type="text"
                        value={streetName}
                        placeholder="Calle 1"
                        onIonChange={e => setStreetName(e.detail.value)}
                        disabled={submitting}
                        required />
                </IonItem>
                {renderErrorMessage('street_name')}
                <IonItem>
                    <IonInput
                        type="text"
                        value={streetNameAlt}
                        placeholder="Calle 2"
                        onIonChange={e => setStreetNameAlt(e.detail.value)}
                        disabled={submitting}
                        required />
                </IonItem>
                {renderErrorMessage('street_name_alt')}
                <IonItem>
                    <IonInput
                        type="text"
                        value={betweenStreets}
                        placeholder="Entre Calles"
                        onIonChange={e => setBetweenStreets(e.detail.value)}
                        disabled={submitting}
                        required />
                </IonItem>
                {renderErrorMessage('between_streets')}
                <IonItem>
                    <IonInput
                        type="text"
                        value={streetNumber}
                        placeholder="Numero"
                        onIonChange={e => setStreetNumber(e.detail.value)}
                        disabled={submitting}
                        required />
                </IonItem>
                {renderErrorMessage('street_number')}
                <IonItem>
                    <IonInput
                        type="text"
                        value={zipCode}
                        placeholder="Codigo Postal"
                        onIonChange={e => setZipCode(e.detail.value)}
                        disabled={submitting}
                        required />
                </IonItem>
                {renderErrorMessage('zip_code')}
                <IonItem>
                    <IonInput
                        type="text"
                        value={countryName}
                        placeholder="Pais"
                        onIonChange={e => setCountryName(e.detail.value)}
                        disabled={submitting}
                        required />
                </IonItem>
                {renderErrorMessage('country_name')}
                <IonItem>
                    <IonInput
                        type="text"
                        value={stateName}
                        placeholder="Provincia"
                        onIonChange={e => setStateName(e.detail.value)}
                        disabled={submitting}
                        required />
                </IonItem>
                {renderErrorMessage('state_name')}
                <IonItem>
                    <IonInput
                        type="text"
                        value={cityName}
                        placeholder="Ciudad"
                        onIonChange={e => setCityName(e.detail.value)}
                        disabled={submitting}
                        required />
                </IonItem>
                {renderErrorMessage('city_name')}

                <IonItem>
                    <IonInput
                        type="text"
                        value={reference}
                        placeholder="Referencia"
                        onIonChange={e => setReference(e.detail.value)}
                        disabled={submitting}
                        required />
                </IonItem>
                {renderErrorMessage('reference')}

                <IonButton
                    expand="block"
                    color="success"
                    onClick={() => estimatePurchaseOrder()}
                    disabled={submitting}
                    expand="block">
                    Ver Resumen de Compra
                </IonButton>

                <IonAlert
                    isOpen={showAlertError}
                    onDidDismiss={() => setShowAlertError(false)}
                    header={'No se puedo realizar la compra'}
                    message={alertMessage || 'Intente nuevamente mas tarde'}
                    buttons={['Aceptar']}
                />

                <IonLoading
                    isOpen={submitting}
                    backdropDismiss={false}
                    keyboardClose={false}
                    message={'Espere por favor...'}
                    duration={5000}
                />
            </IonContent>
        </IonPage >
    )
}