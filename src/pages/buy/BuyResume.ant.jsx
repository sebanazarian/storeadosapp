import React, { useState, useEffect } from 'react'
import { useHistory, useParams } from "react-router-dom"
import {
    IonContent,
    IonPage,
    IonTitle,
    IonToolbar,
    IonBackButton,
    IonButton,
    IonButtons,
    IonItem,
    IonInput,
    IonIcon,
    IonText,
    IonLoading,
    IonAlert,
    IonLabel,
    IonList,
    IonRadioGroup,
    IonListHeader,
    IonRadio,
    IonSpinner,
} from '@ionic/react'

import { closeCircleOutline } from 'ionicons/icons'

import {
    ProductService,
    PaymentMethodService,
    PurchaseOrderService
} from 'services'

import { Title } from 'components'

export const BuyResume = () => {

    const history = useHistory()
    const params = useParams()

    const [errors, setErrors] = useState({})
    const [showAlertError, setShowAlertError] = useState(false)
    const [alertMessage, setAlertMessage] = useState()
    const [submitting, setSubmitting] = useState(false)

    const [product, setProduct] = useState()
    const [purchaseOrder, setPurchaseOrder] = useState()

    useEffect(() => {
        if (!history.location?.state?.product ||
            !history.location?.state?.purchaseOrder ||
            !history.location?.state?.createPurchaseOrder) {
            history.replace(`/${params.id}-${params.slug}`)
            return
        }

        setProduct(history.location?.state?.product)
        setPurchaseOrder(history.location?.state?.purchaseOrder)
    }, [])

    const buy = async () => {
        setSubmitting(true)
        try {
            let { createPurchaseOrder } = history.location.state;
            let purchaseOrder = await PurchaseOrderService.create(createPurchaseOrder);

            console.log(purchaseOrder)
            if(purchaseOrder?.payment_orders[0]?.checkout_url) {
                console.log(purchaseOrder?.payment_orders[0]?.checkout_url);
                window.location.replace(purchaseOrder?.payment_orders[0]?.checkout_url)
            }

        } catch (error) {
            console.error(error)
            if (error?.response?.data?.errors) {
                setErrors(error?.response?.data?.errors)
            } else {
                setAlertMessage(error?.response?.data?.message)
                setShowAlertError(true)
            }
        }
        setSubmitting(false)
    }

    if (!purchaseOrder) {
        return "Cargando";
    }

    const { quantity, amount, unit_price } = purchaseOrder.products[0]
    const { buyer, payment_method, delivery_method } = purchaseOrder;

    return (
        <IonPage>
            <Title>Resumen</Title>
            <IonToolbar>
                <IonButtons slot="start">
                    <IonBackButton defaultHref={`/${params.id}-${params.slug}/buy`} />
                </IonButtons>
                <IonTitle>Resumen</IonTitle>
            </IonToolbar>
            <IonContent>

                <IonItem>
                    <IonLabel><strong>Datos del Comprador</strong></IonLabel>
                </IonItem>
                <IonItem>
                    <IonLabel>Nombre: {buyer.first_name}</IonLabel>
                </IonItem>
                <IonItem>
                    <IonLabel>Apellido: {buyer.last_name}</IonLabel>
                </IonItem>
                <IonItem>
                    <IonLabel>Email: {buyer.email}</IonLabel>
                </IonItem>
                <IonItem>
                    <IonLabel>Telefono: {buyer.phone}</IonLabel>
                </IonItem>

                <IonItem>
                    <IonLabel><strong>Datos del Domicilio de Entrega</strong></IonLabel>
                </IonItem>

                <IonItem>
                    <IonLabel>Calle: {buyer.street_name}</IonLabel>
                </IonItem>
                {
                    buyer.street_name_alt &&
                    <IonItem>
                        <IonLabel>Calle Alternativa: {buyer.street_name_alt}</IonLabel>
                    </IonItem>
                }
                {
                    buyer.between_streets &&
                    <IonItem>
                        <IonLabel>Entre Calles: {buyer.between_streets}</IonLabel>
                    </IonItem>
                }
                <IonItem>
                    <IonLabel>Numero: {buyer.street_number}</IonLabel>
                </IonItem>
                <IonItem>
                    <IonLabel>Codigo Postal: {buyer.zip_code}</IonLabel>
                </IonItem>
                <IonItem>
                    <IonLabel>Pais: {buyer.country_name}</IonLabel>
                </IonItem>
                <IonItem>
                    <IonLabel>Provincia: {buyer.state_name}</IonLabel>
                </IonItem>
                <IonItem>
                    <IonLabel>Ciudad: {buyer.city_name}</IonLabel>
                </IonItem>
                {
                    buyer.reference &&
                    <IonItem>
                        <IonLabel>Referencia: {buyer.reference}</IonLabel>
                    </IonItem>
                }
                <IonItem>
                    <IonLabel><strong>Datos de la compra</strong></IonLabel>
                </IonItem>
                {
                    payment_method &&
                    <IonItem>
                        <IonLabel>Pago: <strong>{payment_method.name}</strong></IonLabel>
                    </IonItem>
                }
                {
                    delivery_method &&
                    <IonItem>
                        <IonLabel>Entrega: <strong>{delivery_method.name}</strong></IonLabel>
                    </IonItem>
                }
                <IonItem>
                    <IonLabel>Producto: {product.name}</IonLabel>
                </IonItem>
                <IonItem>
                    <IonLabel>Cantidad: {quantity}</IonLabel>
                </IonItem>
                <IonItem>
                    <IonLabel>Precio: $ {Intl.NumberFormat('es', { minimumFractionDigits: 2 }).format(purchaseOrder.products_amount)}</IonLabel>
                </IonItem>
                {
                    purchaseOrder.shipping_amount &&
                    <IonItem>
                        <IonLabel>Costo de envio: $ {Intl.NumberFormat('es', { minimumFractionDigits: 2 }).format(purchaseOrder.shipping_amount)}</IonLabel>
                    </IonItem>
                }
                <IonItem>
                    <IonLabel><strong>Total: $ {Intl.NumberFormat('es', { minimumFractionDigits: 2 }).format(purchaseOrder.amount)}</strong></IonLabel>
                </IonItem>

                <IonButton
                    expand="block"
                    color="danger"
                    onClick={() => buy()}
                    disabled={submitting}
                    expand="block">
                    Finalizar Comprar
                </IonButton>

                <IonAlert
                    isOpen={showAlertError}
                    onDidDismiss={() => setShowAlertError(false)}
                    header={'No se puedo realizar la compra'}
                    message={alertMessage || 'Intente nuevamente mas tarde'}
                    buttons={['Aceptar']}
                />

                <IonLoading
                    isOpen={submitting}
                    backdropDismiss={false}
                    keyboardClose={false}
                    message={'Espere por favor...'}
                    duration={5000}
                />
            </IonContent>
        </IonPage>
    )
}