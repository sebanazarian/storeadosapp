import React, { useState, useEffect } from 'react'
import { useHistory, useParams } from "react-router-dom"
import {
    IonIcon,
    IonText,
    IonLoading,
    IonAlert
} from '@ionic/react'

import { closeCircleOutline } from 'ionicons/icons'

import {
    ProductService,
    PaymentMethodService,
    PurchaseOrderService
} from 'services'

import { Spinner, SelectOptions, App, Content, Button, Input } from 'components'

export const BuyDelivery = () => {

    const history = useHistory()
    const params = useParams()

    const [errors, setErrors] = useState({})
    const [showAlertError, setShowAlertError] = useState(false)
    const [alertMessage, setAlertMessage] = useState()
    const [submitting, setSubmitting] = useState(false)
    const [loading, setLoading] = useState(false)

    const [product, setProduct] = useState()
    const [payment, setPayment] = useState([])
    const [deliveryMethod, setDeliveryMethod] = useState([])

    const [quantity, setQuantity] = useState(1)
    const [streetName, setStreetName] = useState('Mendoza')
    const [streetNameAlt, setStreetNameAlt] = useState('')
    const [betweenStreets, setBetweenStreets] = useState('')
    const [streetNumber, setStreetNumber] = useState(3040)
    const [zipCode, setZipCode] = useState(3000)
    const [countryName, setCountryName] = useState('Argentina')
    const [stateName, setStateName] = useState('Santa Fe')
    const [cityName, setCityName] = useState('Santa Fe')
    const [reference, setReference] = useState('')
    
    const [valid, setValid] = useState(false)

    useEffect(() => {
        if (!history.location.state) {
            history.replace(`/${params.id}-${params.slug}`)
            return
        }

        let { product, quantity, payment, delivery } = history.location.state

        if (!product || !quantity) {
            history.replace(`/${params.id}-${params.slug}`)
            return
        }

        if (!payment) {
            history.replace(`/${params.id}-${params.slug}/payment`, {
                product,
                quantity 
            })
            return
        }

        setProduct(product)
        setQuantity(quantity)
        setPayment(payment)
        if(delivery) {
            setDeliveryMethod(delivery.deliveryMethod)
            setStreetName(delivery.streetName)
            setStreetNameAlt(delivery.streetNameAlt)
            setBetweenStreets(delivery.betweenStreets)
            setStreetNumber(delivery.streetNumber)
            setZipCode(delivery.zipCode)
            setCountryName(delivery.countryName)
            setStateName(delivery.stateName)
            setCityName(delivery.cityName)
            setReference(delivery.reference)
        }
    }, [])

    const estimatePurchaseOrder = async () => {
        setSubmitting(true)
        try {
            let data = {
                delivery_method: deliveryMethod[0],
                payment_method: payment.paymentMethod[0],
                first_name: payment.firstName,
                last_name: payment.lastName,
                email: payment.email,
                phone: payment.phone,
                street_name: streetName,
                street_name_alt: streetNameAlt,
                between_streets: betweenStreets,
                street_number: streetNumber,
                zip_code: zipCode,
                country_name: countryName,
                state_name: stateName,
                city_name: cityName,
                reference,
                products: [
                    {
                        product_id: product.id,
                        quantity
                    }
                ]
            }

            let purchaseOrder = await PurchaseOrderService.estimate(data)

            history.push(`/${params.id}-${params.slug}/resume`, {
                product, 
                quantity, 
                payment,
                delivery: {
                    deliveryMethod,
                    streetName,
                    streetNameAlt,
                    betweenStreets,
                    streetNumber,
                    zipCode,
                    countryName,
                    stateName,
                    cityName,
                    reference
                },
                purchaseOrder,
                createPurchaseOrder: data,
            })
        } catch (error) {
            console.error(error)
            if (error?.response?.data?.errors) {
                setErrors(error?.response?.data?.errors)
            } else {
                setAlertMessage(error?.response?.data?.message)
                setShowAlertError(true)
            }
        }
        setSubmitting(false)
    }

    useEffect(() => {
        setValid(
            streetName.trim() !== '' &&
            betweenStreets.trim() !== '' &&
            streetNumber.toString().trim() !== '' &&
            zipCode.toString().trim() !== '' &&
            countryName.trim() !== '' &&
            stateName.trim() !== '' &&
            cityName.trim() !== '' &&
            reference.trim() !== ''&&
            deliveryMethod.length == 1
        )
    }, [deliveryMethod, streetName, betweenStreets, streetNumber,  zipCode, countryName, stateName, cityName, reference])

    const icons = {
        1: 'handshake-o',
        2: 'credit-card-alt',
        3: 'money',
    }
    
    return (
        <App title="Datos de entrega" titlebar onBack={() => history.push(`/${params.id}-${params.slug}/payment`, { 
            product, 
            quantity, 
            payment,
            delivery: {
                deliveryMethod,
                streetName,
                streetNameAlt,
                betweenStreets,
                streetNumber,
                zipCode,
                countryName,
                stateName,
                cityName,
                reference
            }
            })}>
            <Content>
                <div className="row">
                    <div className="col-12 col-sm-12 col-md-12 col-lg-12">
                        <article className="App-DatosPago wow fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.5s">
                            <form className="FormularioLogin FormularioBase" action="#">
                                {
                                    product &&
                                        <SelectOptions
                                            title="Formas de entrega"
                                            icons={icons}
                                            value={deliveryMethod}
                                            options={product.delivery_methods}
                                            onChange={value => setDeliveryMethod(value)}
                                            errors={errors['delivery_method']} />
                                }
                                <h2 className="Title FontTitle1 mb-3 mt-3">Datos del domicilio</h2>
                                <Input
                                    type="text"
                                    value={streetName}
                                    placeholder="Calle 1"
                                    onChange={e => setStreetName(e.target.value)}
                                    disabled={submitting}
                                    errors={errors['street_name']} />
                                <Input
                                    type="text"
                                    value={streetNameAlt}
                                    placeholder="Calle 2"
                                    onChange={e => setStreetNameAlt(e.target.value)}
                                    disabled={submitting}
                                    errors={errors['street_name_alt']} />
                                <Input
                                    type="text"
                                    value={betweenStreets}
                                    placeholder="Entre Calles"
                                    onChange={e => setBetweenStreets(e.target.value)}
                                    disabled={submitting}
                                    errors={errors['between_streets']} />
                                <Input
                                    type="text"
                                    value={streetNumber}
                                    placeholder="Numero"
                                    onChange={e => setStreetNumber(e.target.value)}
                                    disabled={submitting}
                                    errors={errors['street_number']} />
                                <Input
                                    type="text"
                                    value={zipCode}
                                    placeholder="Codigo Postal"
                                    onChange={e => setZipCode(e.target.value)}
                                    disabled={submitting}
                                    errors={errors['zip_code']} />
                                <Input
                                    type="text"
                                    value={countryName}
                                    placeholder="Pais"
                                    onChange={e => setCountryName(e.target.value)}
                                    disabled={submitting}
                                    errors={errors['country_name']} />
                                <Input
                                    type="text"
                                    value={stateName}
                                    placeholder="Provincia"
                                    onChange={e => setStateName(e.target.value)}
                                    disabled={submitting}
                                    errors={errors['state_name']} />
                                <Input
                                    type="text"
                                    value={cityName}
                                    placeholder="Ciudad"
                                    onChange={e => setCityName(e.target.value)}
                                    disabled={submitting}
                                    errors={errors['city_name']} />

                                <Input
                                    type="text"
                                    value={reference}
                                    placeholder="Referencia"
                                    onChange={e => setReference(e.target.value)}
                                    disabled={submitting}
                                    errors={errors['reference']} />

                                <Button
                                    color="primary"
                                    onClick={() => estimatePurchaseOrder()}
                                    disabled={!valid || submitting}>
                                    Continuar
                                </Button>
                            </form>
                        </article>
                    </div>
                </div>
            </Content>

            <IonAlert
                isOpen={showAlertError}
                onDidDismiss={() => setShowAlertError(false)}
                header={'No se puedo realizar la compra'}
                message={alertMessage || 'Intente nuevamente mas tarde'}
                buttons={['Aceptar']}
            />

            <IonLoading
                isOpen={submitting}
                backdropDismiss={false}
                keyboardClose={false}
                message={'Espere por favor...'}
            />
        </App >
    )
}
