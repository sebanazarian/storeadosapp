import React, { useState, useEffect, useRef } from 'react'
import { useHistory } from "react-router-dom"
import {
    IonContent,
    IonPage,
    IonTitle,
    IonToolbar,
    IonBackButton,
    IonButtons,
    IonItem,
    IonLabel,
    IonIcon,
    IonCardContent,
    IonCard,
    IonImg,
    IonButton,
    IonSearchbar,
    IonSpinner,
    IonCardHeader,
    IonCardSubtitle,
    IonCardTitle,
    IonInfiniteScroll,
    IonInfiniteScrollContent,
    IonRefresher,
    IonRefresherContent
} from '@ionic/react'

import { List, Map, fromJS } from 'immutable'

import config from 'config'

import { ProductService } from 'services'

import { Title } from 'components'

export const ListProducts = () => {

    const history = useHistory()

    const refresh = useRef()
    const infiniteScroll = useRef()
    const timer = useRef()
    const [products, setProducts] = useState(new List())
    const [curretPage, setCurrentPage] = useState(0)
    const [search, setSearch] = useState()
    const [submitting, setSubmitting] = useState(false)
    const [more, setMore] = useState(false)

    const loadProducts = async (page, isRefresh) => {
        try {
            let { data, meta } = await ProductService.listByUser({
                page,
                query: search
            })

            setCurrentPage(page)
            setMore(meta.to < meta.total)

            data = fromJS(data);

            setProducts(
                page == 1 ? data : products.concat(data)
            )
        } catch (error) {
            console.error(error)
        }

        infiniteScroll.current && infiniteScroll.current.complete()

        isRefresh && refresh.current && refresh.current.complete()

        setSubmitting(false)
    }
    const changeState = async (product, index) => {
        try {
            let { id, status } = product

            if(status === 'paused') {
              await ProductService.publish(id)
              product.status = 'published'
            } else {
              await ProductService.pause(id)
              product.status = 'paused'
            }
 
            setProducts(products.set(index, new Map(product)))
        } catch (error) {
            console.error(error)
        }
    }
    const moreProducts = async () => {
        let nextPage = curretPage + 1;
        loadProducts(nextPage);
    }

    useEffect(() => {
        setSubmitting(true)
        loadProducts(1);
    }, [])

    useEffect(() => {
        timer.current && clearTimeout(timer.current)
        timer.current = setTimeout(() => {
            setSubmitting(true)
            loadProducts(1);
        }, 500)

        return () => {
            timer.current && clearTimeout(timer.current)
        }
    }, [search])


    const renderProduct = (product, index) => {

        return (
            <IonCard key={product.id}>
                {
                    product.photo &&
                    <IonImg src={`${config.url}/images/medium/${product.photo}`} />
                }
                <IonCardHeader>
                    <IonCardSubtitle>{product.category.name}</IonCardSubtitle>
                    <IonCardTitle>{product.name}</IonCardTitle>
                </IonCardHeader>
                <IonItem>
                    <IonLabel>$ {Intl.NumberFormat('es', { minimumFractionDigits: 2 }).format(product.price)}</IonLabel>
                    <IonLabel>Vistas: {product.views || 0}</IonLabel>
                </IonItem>
                <IonCardContent>
                    {product.detail}
                </IonCardContent>
                {
                    product.status == 'paused'
                        ? <IonButton expand="block" color="dark" onClick={() => changeState(product, index)} disabled={submitting} expand="block">Activar</IonButton>
                        : <IonButton expand="block" color="dark" onClick={() => changeState(product, index)} disabled={submitting} expand="block">Pausar</IonButton>
                }
                <IonButton onClick={e => history.push(`products/${product.id}`, { product })} expand="block">Ver</IonButton>
                <IonButton onClick={e => history.push(`products/${product.id}/edit`, { product })} expand="block" disabled={product.status == 'published'}>Editar</IonButton>
            </IonCard>
        )
    }

    return (
        <IonPage>
            <Title>Mis Publicaciones</Title>
            <IonToolbar>
                <IonButtons slot="start">
                    <IonBackButton defaultHref="/" />
                </IonButtons>
                <IonTitle>Mis Publicaciones</IonTitle>
            </IonToolbar>
            <IonContent>
                <IonRefresher slot="fixed" onIonRefresh={() => loadProducts(1, true)} ref={refresh}>
                    <IonRefresherContent></IonRefresherContent>
                </IonRefresher>
                <IonItem>
                    <IonSearchbar onIonChange={e => setSearch(e.detail.value)} placeholder="Buscar en mis articulos"></IonSearchbar>
                </IonItem>
                <IonButton color="primary" onClick={() => history.push('/products/create')} expand="block">
                    <IonIcon slot="start" icon='' />
                    Publicar nuevo Articulo
                </IonButton>
                {
                    submitting
                        ? <IonSpinner style={{ 'margin': 'calc(50% - 14px)' }} name="crescent" />
                        : products.map((product, index) => renderProduct(product.toJS(), index))
                }
                <IonInfiniteScroll
                    threshold="100px"
                    onIonInfinite={() => moreProducts()}
                    disabled={!more}
                    ref={infiniteScroll}
                >
                    <IonInfiniteScrollContent
                        loadingSpinner="crescent"
                        loadingText="Cargando..." />
                </IonInfiniteScroll>
            </IonContent>
        </IonPage>
    )
}