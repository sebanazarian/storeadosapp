import React, { useState, useEffect } from 'react'
import { useHistory } from "react-router-dom"
import {
    IonContent,
    IonPage,
    IonTitle,
    IonToolbar,
    IonBackButton,
    IonButton,
    IonButtons,
    IonItem,
    IonInput,
    IonIcon,
    IonText,
    IonLoading,
    IonAlert,
    IonSelect,
    IonSelectOption,
    IonLabel,
    IonTextarea,
    IonCard,
    IonItemDivider,
    IonImg,
} from '@ionic/react'

import { isPlatform } from '@ionic/react';

import { useFilesystem, base64FromPath } from '@ionic/react-hooks/filesystem';
import { useCamera } from '@ionic/react-hooks/camera';

import { closeCircleOutline } from 'ionicons/icons';

import { 
    ProductService,
    CategoryService,
    DeliveryMethodService
} from 'services'

import { Title, InputFile } from 'components'

import { 
    Plugins,
    Capacitor,
    CameraResultType,
    CameraSource,
    CameraPhoto,
    FilesystemDirectory 
} from "@capacitor/core";

export const CreateProduct = () => {

    const history = useHistory()

    const { getPhoto } = useCamera()

    const { deleteFile, getUri, readFile, writeFile } = useFilesystem()

    const [categories, setCategories] = useState([])
    const [subCategories, setSubCategories] = useState([])
    const [deliveryMethods, setDeliveryMethods] = useState([])

    const [name, setName] = useState()
    const [detail, setDetail] = useState()
    const [price, setPrice] = useState()
    const [stock, setStock] = useState()
    const [category, setCategory] = useState()
    const [subCategory, setSubCategory] = useState()
    const [deliveryMethod, setDeliveryMethod] = useState([])
    const [photos, setPhotos] = useState([])

    const [errors, setErrors] = useState({})
    const [showAlertError, setShowAlertError] = useState(false);
    const [alertMessage, setAlertMessage] = useState();
    const [submitting, setSubmitting] = useState(false)
    const [loading, setLoading] = useState(true)
    const [progress, setProgress] = useState(false)

    const load = async () => {
        try {
            let [
                categories,
                deliveryMethods,
            ] = await Promise.all([
                CategoryService.all(),
                DeliveryMethodService.all()
            ])

            setCategories(categories)
            setDeliveryMethods(deliveryMethods)
        } catch (error) {
            // Error
        }

        setLoading(false)
    }

    const loadSubCategories = async () => {
        try {
            if (category) {
                let categories = await CategoryService.getChilds(category);
                setSubCategory(null)
                setSubCategories(categories)
            }
        } catch (error) {
            // Error
        }
    }

    useEffect(() => { load() }, [])
    useEffect(() => { loadSubCategories() }, [category])

    const onUploading = (e) => {
        console.log("onUploading", JSON.stringify(e));
        setProgress(e.loaded / e.total)
    }

    const publish = async () => {
        setSubmitting(true)

        try {
            await ProductService.createAndPhotosUpload({
                name,
                category_id: subCategory || category,
                delivery_methods: deliveryMethod,
                detail,
                price,
                stock
            }, photos, onUploading)

            setSubmitting(false)
            history.replace('/');
            return

        } catch (error) {
            console.log("error", error)
            if (error?.response?.data?.errors) {
                setErrors(error?.response?.data?.errors)
            } else {
                setAlertMessage(error?.response?.data?.message)
                setShowAlertError(true)
            }
        }
        setProgress(false)
        setSubmitting(false)
    }

    const takePicture = async () => {
        const photo = await getPhoto({
            quality: 100,
            resultType: CameraResultType.Uri,
            source: CameraSource.Prompt,
        })

        let response = await fetch(photo.webPath);

        setPhotos([...photos, {
            url: photo.webPath,
            blob: await response.blob()
        }])

        setLoading(true)
    }

    const addPhoto = (file) => {
        setPhotos([...photos, {
            url: URL.createObjectURL(file),
            blob: file
        }])
    }

    const deletePhoto = (index) => {
        setPhotos(photos.filter((_, i) => index != i))
    }

    const renderErrorMessage = (field) => {
        return errors[field] && errors[field].map((message, index) => (
            <IonText key={field + index} color="danger">
                <p style={{ display: 'flex', alignItems: 'center' }}>
                    <IonIcon icon={closeCircleOutline} style={{ fontSize: '25px', marginLeft: '20px', marginRight: '10px' }} />
                    {message}
                </p>
            </IonText>
        ))
    }

    return (
        <IonPage>
            <Title>Nuevo Producto</Title>
            <IonToolbar>
                <IonButtons slot="start">
                    <IonBackButton defaultHref="/" />
                </IonButtons>
                <IonTitle>Nuevo Producto</IonTitle>
            </IonToolbar>
            <IonContent>
                <IonItemDivider />
                <IonItem>
                    <IonText><strong>Fotos</strong></IonText>
                </IonItem>
                {
                    photos.map((photo, index) =>
                        <IonCard key={index} style={{ textAlign: 'center' }}>
                            <IonImg src={photo.url}
                                style={{
                                    height: '200px',
                                    objectFit: 'cover'
                                }}
                                onIonError={() => setLoading(false)}
                                onIonImgDidLoad={() => setLoading(false)}
                            />
                            <IonButton expand="block" onClick={() => deletePhoto(index)}>Eliminar</IonButton>
                        </IonCard>
                    )
                }
                <InputFile 
                        onFile={addPhoto} 
                        render={(props) => <IonButton onClick={props.onClick} expand="block">Agregar Foto</IonButton>}
                    />
                {Capacitor.isPluginAvailable('camera') && <IonButton onClick={e => takePicture()} expand="block">Camara</IonButton>}
                <IonItem>
                    <IonText><strong>Datos del Producto</strong></IonText>
                </IonItem>
                <IonItem>
                    <IonInput
                        type="text"
                        value={name}
                        placeholder="Nombre"
                        onIonChange={e => setName(e.detail.value)}
                        disabled={submitting}
                        required />
                </IonItem>
                {renderErrorMessage('name')}
                <IonItem>
                    <IonLabel>Categoria</IonLabel>
                    <IonSelect
                        value={category}
                        onIonChange={e => setCategory(e.detail.value)}
                        disabled={!deliveryMethods.length}
                    >
                        {categories.map(category => <IonSelectOption key={category.id} value={category.id}>{category.name}</IonSelectOption>)}
                    </IonSelect>
                </IonItem>
                <IonItem>
                    <IonLabel>Subcategoria</IonLabel>
                    <IonSelect
                        value={subCategory}
                        onIonChange={e => setSubCategory(e.detail.value)}
                        disabled={!category || subCategories.length == 0}
                    >
                        {subCategories.map(category => <IonSelectOption key={category.id} value={category.id}>{category.name}</IonSelectOption>)}
                    </IonSelect>
                </IonItem>
                {renderErrorMessage('category_id')}
                <IonItem>
                    <IonTextarea
                        type="text"
                        value={detail}
                        placeholder="Detalle"
                        onIonChange={e => setDetail(e.detail.value)}
                        disabled={submitting}
                        required />
                </IonItem>
                {renderErrorMessage('detail')}
                <IonItem>
                    <IonInput
                        type="number"
                        value={price}
                        placeholder="Precio"
                        onIonChange={e => setPrice(e.detail.value)}
                        disabled={submitting}
                        required />
                </IonItem>
                {renderErrorMessage('price')}
                <IonItem>
                    <IonInput
                        type="number"
                        value={stock}
                        placeholder="Stock"
                        onIonChange={e => setStock(e.detail.value)}
                        disabled={submitting}
                        required />
                </IonItem>
                {renderErrorMessage('stock')}
                <IonItem>
                    <IonLabel>Forma de entrega</IonLabel>
                    <IonSelect
                        value={deliveryMethod}
                        multiple={true}
                        onIonChange={e => setDeliveryMethod(e.detail.value)}
                        disabled={!deliveryMethods.length}
                    >
                        {deliveryMethods.map(deliveryMethod => <IonSelectOption key={deliveryMethod.id} value={deliveryMethod.id}>{deliveryMethod.name}</IonSelectOption>)}
                    </IonSelect>
                </IonItem>
                {renderErrorMessage('delivery_methods')}
                <IonButton expand="block" onClick={() => publish()} color="dark" disabled={submitting}>Publicar</IonButton>

                <IonAlert
                    isOpen={showAlertError}
                    onDidDismiss={() => setShowAlertError(false)}
                    header={'No se puedo crear el producto'}
                    message={alertMessage || 'Intente nuevamente mas tarde'}
                    buttons={['Aceptar']}
                />

                <IonLoading
                    isOpen={loading}
                    backdropDismiss={false}
                    keyboardClose={false}
                    message={'Cargando...'}
                    duration={5000}
                />

                <IonLoading
                    isOpen={submitting}
                    backdropDismiss={false}
                    keyboardClose={false}
                    message={progress || 'Espere por favor...'}
                />
            </IonContent>
        </IonPage>
    )
}