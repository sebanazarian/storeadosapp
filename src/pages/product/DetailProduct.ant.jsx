import React, { useState, useEffect } from 'react'
import { useHistory, useParams } from "react-router-dom"
import {
    IonContent,
    IonPage,
    IonTitle,
    IonToolbar,
    IonBackButton,
    IonButton,
    IonButtons,
    IonItem,
    IonLabel,
    IonImg
} from '@ionic/react'

import { Plugins } from '@capacitor/core';

import { ProductService } from 'services'

import { Title } from 'components'

import config from '../../config'

const { Share, Browser } = Plugins;

export const DetailProduct = () => {

    const history = useHistory()
    const params = useParams()

    const [errors, setErrors] = useState({})
    const [showAlertError, setShowAlertError] = useState(false);
    const [submitting, setSubmitting] = useState(false)

    const [product, setProduct] = useState(undefined)

    const loadProduct = async () => {
        try {
            let product = await ProductService.detail(params.id)
            setProduct(product)
        } catch (error) {
            console.error(error)
        }
    }

    const openInBrowser = async () => {
        await Browser.open({ url: product.url });
    }


    const share = async () => {
        await Share.share({
            title: product.name,
            text: product.detail,
            url: product.url,
            dialogTitle: 'Compartir Producto'
        });
    }


    useEffect(() => {
        setProduct(history.location?.state?.product)
        loadProduct()
    }, [])

    return (
        <IonPage>
            <Title>{product?.name || ''}</Title>
            <IonToolbar>
                <IonButtons slot="start">
                    <IonBackButton defaultHref="/products" />
                </IonButtons>
                <IonTitle>{product?.name || ''}</IonTitle>
            </IonToolbar>
            {
                product &&
                <IonContent>
                    {product.gallery.map((photo, index) => <IonImg key={index} src={`${config.url}/images/medium/${photo}`} />)}
                    <IonItem>
                        <IonLabel>{product.category.name}</IonLabel>
                    </IonItem>
                    <IonItem>
                        <IonLabel>$ {Intl.NumberFormat('es', { minimumFractionDigits: 2 }).format(product.price)}</IonLabel>
                    </IonItem>
                    <p>
                        {product.detail}
                    </p>
                    <IonButton onClick={() => openInBrowser()} expand="block">Ver Publicacion</IonButton>
                    <IonButton onClick={() => share()} expand="block">Compartir</IonButton>
                </IonContent>
            }
        </IonPage>
    )
}