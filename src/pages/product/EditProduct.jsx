import React, { useState, useEffect } from 'react'
import { useHistory, useParams } from "react-router-dom"
import {
    IonLoading,
    IonAlert,
    IonGrid,
    IonRow,
    IonCol,
} from '@ionic/react'

import { useCamera } from '@ionic/react-hooks/camera'

import { closeCircleOutline, save } from 'ionicons/icons'

import {
    Plugins,
    Capacitor,
    CameraResultType,
    CameraSource
} from "@capacitor/core"

import {
    ProductService,
    CategoryService,
    DeliveryMethodService
} from 'services'

import {
    App,
    Content,
    InputFile,
    Input,
    TextArea,
    ButtonBoxGradent,
    Button,
    ImageAndDelete,
    Select,
    SelectOptions
} from 'components'

const { Share, Browser } = Plugins

export const EditProduct = () => {

    const history = useHistory()
    const params = useParams()

    const { getPhoto } = useCamera()

    const [categories, setCategories] = useState([])
    const [subCategories, setSubCategories] = useState([])
    const [deliveryMethods, setDeliveryMethods] = useState([])

    const [name, setName] = useState('')
    const [detail, setDetail] = useState('')
    const [price, setPrice] = useState()
    const [stock, setStock] = useState()
    const [category, setCategory] = useState()
    const [subCategory, setSubCategory] = useState()
    const [deliveryMethod, setDeliveryMethod] = useState([])
    const [gallery, setGallery] = useState([])
    const [photos, setPhotos] = useState([])
    const [product, setProduct] = useState(undefined)

    const [errors, setErrors] = useState({})
    const [showAlertError, setShowAlertError] = useState(false)
    const [alertMessage, setAlertMessage] = useState();
    const [submitting, setSubmitting] = useState(false)
    const [loading, setLoading] = useState(true)
    const [progress, setProgress] = useState(false)
    const [valid, setValid] = useState(false)
    const [changePhotos, setChangePhotos] = useState(false)

    useEffect(() => { load() }, [])
    useEffect(() => { loadSubCategories() }, [category])

    const load = async () => {
        try {
            let [
                product,
                gallery,
                categories,
                deliveryMethods,
            ] = await Promise.all([
                ProductService.detail(params.id),
                ProductService.listPhotos(params.id),
                CategoryService.all(),
                DeliveryMethodService.all()
            ])

            setProduct(product)
            setCategories(categories)
            setDeliveryMethods(deliveryMethods)
            setGallery(gallery)
            setName(product.name)
            setDetail(product.detail)
            setPrice(product.price)
            setStock(product.stock)
            setDeliveryMethod(product.delivery_methods.map(e => e.id))
            setCategory(product.category?.category?.id)

            if (product.category?.category?.id) {
                let categories = await CategoryService.getChilds(product.category?.category?.id)
                setSubCategories(categories)
                setSubCategory(product.category?.id)
            }
        } catch (error) {
            console.error(error)
        }

        setLoading(false)
    }

    const onUploading = (e) => {
        console.log("onUploading", JSON.stringify(e));
        setProgress(e.loaded / e.total)
    }

    const save = async () => {
        setSubmitting(true)

        try {
            let data = {
                name,
                detail,
                price,
                stock,
                delivery_methods: deliveryMethod,
                category: subCategory
            }

            await ProductService.updateAndPhotosUpload(
                params.id,
                data,
                photos,
                onUploading
            )

            await Promise.all(
                gallery.filter(photo => photo.delete)
                    .map(photo => ProductService.deletePhoto(product.id, photo.id))
            )

            setSubmitting(false)
            history.replace('/');
            return

        } catch (error) {
            console.log(error)
            if (error?.response?.data?.errors) {
                setErrors(error?.response?.data?.errors)
            } else {
                setAlertMessage(error?.response?.data?.message)
                setShowAlertError(true)
            }
        }

        setSubmitting(false)
    }

    const includes = (array1, array2) => {
        if (array1.length != array2.length)
            return false

        for (let key of array1) {
            if (!array2.includes(key)) {
                return false
            }
        }

        return true
    }

    useEffect(() => {
        setValid(
            name.trim() !== '' &&
            category > 0 &&
            subCategory > 0 &&
            detail.trim() !== '' &&
            price > 0 &&
            stock > 0 &&
            deliveryMethod.length > 0 && (
                product.name != name ||
                product.category.id != subCategory ||
                product.detail != detail ||
                product.price != price ||
                product.stock != stock ||
                !includes(deliveryMethod, product.delivery_methods.map(d => d.id)) ||
                changePhotos
            )
        )
    }, [name, category, subCategory, detail, price, stock, deliveryMethod, photos, changePhotos])


    const loadSubCategories = async () => {
        try {
            if (category && product.category?.category?.id != category) {
                let categories = await CategoryService.getChilds(category)
                
                setSubCategory(null)
                setSubCategories(categories)
            }
        } catch (error) {
            // Error
        }
    }

    const takePicture = async () => {
        const photo = await getPhoto({
            quality: 100,
            resultType: CameraResultType.Uri,
            source: CameraSource.Prompt,
        })

        let response = await fetch(photo.webPath)

        setPhotos([...photos, {
            url: photo.webPath,
            blob: await response.blob()
        }])

        setLoading(true)
        setChangePhotos(true)
    }

    const addPhotos = (files) => {
        files = [...files].map(file => ({
            url: URL.createObjectURL(file),
            blob: file
        }))

        setPhotos([...photos, ...files])
        setChangePhotos(true)
    }

    const deleteGalleryPhoto = async (photo, index) => {
        setGallery(gallery.map(p => (photo.id == p.id) ? { ...p, delete: true } : p))
        setChangePhotos(true)
    }

    const deletePhoto = (index) => {
        setPhotos(photos.filter((_, i) => index != i))
        setChangePhotos(true)
    }

    const icons = {
        1: 'credit-card-alt',
        2: 'handshake-o'
    }

    return (
        <App title="Editar Articulo" titlebar icon={true ? 'camera' : ''} onButton={e => takePicture()}>
            <Content className="App-MiCuenta-DatosFormularios">
                <div className="row">
                    <div className="col-12 col-sm-12 col-md-12 col-lg-12">
                        <article className="App-DatosPago wow fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.5s">
                            <IonGrid fixed>
                                <IonRow>
                                    {
                                        gallery.filter(photo => !photo.delete).map((photo, index) =>
                                            <IonCol sizeMd="4" sizeSm="6" sizeXl="4" sizeXs="12" key={index}>
                                                <ImageAndDelete
                                                    path={photo.photo}
                                                    onDelete={() => deleteGalleryPhoto(photo, index)} />
                                            </IonCol>
                                        )
                                    }

                                    {
                                        photos.map((photo, index) =>
                                            <IonCol sizeMd="4" sizeSm="6" sizeXl="4" sizeXs="12" key={index}>
                                                <ImageAndDelete
                                                    src={photo.url}
                                                    onError={() => setLoading(false)}
                                                    onDidLoad={() => setLoading(false)}
                                                    onDelete={() => deletePhoto(index)} />
                                            </IonCol>
                                        )
                                    }

                                </IonRow>
                            </IonGrid>

                            <InputFile
                                onFiles={addPhotos}
                                render={(props) => (
                                    <div className="App-DatosPago-Forma App-DatosPago-Fotos">
                                        <ButtonBoxGradent onClick={props.onClick}>Agregar Fotos</ButtonBoxGradent>
                                        <p className="Text"><strong>Fotos {photos.length}/10:</strong> Primero elige la foto principal del anuncio.</p>
                                    </div>
                                )}
                            />

                            <div className="App-MiCuenta-DatosFormularios mt-4">
                                <div className="App-MiCuenta-DatosFormularios-Box mb-4 wow fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.5s">
                                    <form className="FormularioLogin FormularioBase" action="#">
                                        <Input
                                            type="text"
                                            value={name}
                                            placeholder="Titulo"
                                            onChange={e => setName(e.target.value)}
                                            disabled={submitting}
                                            errors={errors['name']} />

                                        <Select
                                            empty
                                            placeholder="Categoria"
                                            value={category}
                                            onChange={e => setCategory(e.target.value)}
                                            disabled={!categories.length}>
                                            {categories.map(category => <option key={category.id} value={category.id}>{category.name}</option>)}
                                        </Select>

                                        {
                                            category && subCategories.length > 0 && <Select
                                                empty
                                                placeholder="Sub Categoria"
                                                errors={errors['category_id']}
                                                value={subCategory}
                                                onChange={e => setSubCategory(e.target.value)}
                                                disabled={!subCategories.length}>
                                                {subCategories.map(category => <option key={category.id} value={category.id}>{category.name}</option>)}
                                            </Select>
                                        }

                                        <TextArea
                                            type="text"
                                            value={detail}
                                            placeholder="Descripción adicional"
                                            onChange={e => setDetail(e.target.value)}
                                            disabled={submitting}
                                            errors={errors['detail']}
                                            rows={10}
                                            cols={30} />

                                        <Input
                                            type="text"
                                            value={price}
                                            placeholder="Precio"
                                            onChange={e => setPrice(e.target.value)}
                                            disabled={submitting}
                                            errors={errors['price']} />
                                        <Input
                                            type="text"
                                            value={stock}
                                            placeholder="Stock"
                                            onChange={e => setStock(e.target.value)}
                                            disabled={submitting}
                                            errors={errors['stock']} />

                                        <SelectOptions
                                            multiple
                                            title="Formas de Entrega"
                                            icons={icons}
                                            value={deliveryMethod}
                                            options={deliveryMethods}
                                            onChange={value => setDeliveryMethod(value)}
                                            errors={errors['delivery_methods']} />

                                        <Button mt={3} onClick={() => save()} color="primary" disabled={!valid || submitting}>Guarda cambios</Button>
                                    </form>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </Content>

            <IonAlert
                isOpen={showAlertError}
                onDidDismiss={() => setShowAlertError(false)}
                header={'No se puedo actualizar el producto'}
                message={alertMessage || 'Intente nuevamente mas tarde'}
                buttons={['Aceptar']}
            />

            <IonLoading
                isOpen={loading}
                backdropDismiss={false}
                keyboardClose={false}
                message={'Cargando...'}
                duration={5000}
            />

            <IonLoading
                isOpen={submitting}
                backdropDismiss={false}
                keyboardClose={false}
                message={progress || 'Espere por favor...'}
            />

        </App>
    )
}