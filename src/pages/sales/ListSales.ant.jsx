import React, { useState, useEffect, useRef } from 'react'
import { useHistory } from "react-router-dom"
import {
    IonContent,
    IonPage,
    IonTitle,
    IonToolbar,
    IonBackButton,
    IonButtons,
    IonItem,
    IonLabel,
    IonIcon,
    IonCardContent,
    IonCard,
    IonImg,
    IonButton,
    IonSearchbar,
    IonSpinner,
    IonCardHeader,
    IonCardSubtitle,
    IonCardTitle,
    IonInfiniteScroll,
    IonInfiniteScrollContent,
    IonRefresher,
    IonRefresherContent
} from '@ionic/react'

import { parseJSON, format } from 'date-fns'

import { PurchaseOrderService } from 'services'

import { Title } from 'components'

import { PurchaseOrder } from 'common'

import config from 'config'

export const ListSales = () => {
    const history = useHistory()

    const refresh = useRef()
    const infiniteScroll = useRef()
    const timer = useRef()

    const [sales, setSales] = useState([])
    const [currentPage, setCurrentPage] = useState(0)
    const [search, setSearch] = useState()
    const [submitting, setSubmitting] = useState(false)
    const [more, setMore] = useState(false)
    const [meta, setMeta] = useState({})

    const loadSales = async (page, isRefresh) => {
        try {
            let { data, meta } = await PurchaseOrderService.list({
                page,
                query: search
            })

            setCurrentPage(page)
            setMeta(meta)
            setMore(meta.to < meta.total)
            setSales(
                page == 1 ? data : [...sales, ...data]
            )
        } catch (error) {
            console.error(error)
        }

        infiniteScroll.current && infiniteScroll.current.complete()

        isRefresh && refresh.current && refresh.current.complete()

        setSubmitting(false)
    }

    const moreSales = async () => {
        let nextPage = currentPage + 1;
        loadSales(nextPage);
    }

    useEffect(() => {
        setSubmitting(true)
        loadSales(1);
    }, [])

    useEffect(() => {
        timer.current && clearTimeout(timer.current)
        timer.current = setTimeout(() => {
            setSubmitting(true)
            loadSales(1);
        }, 500)

        return () => {
            timer.current && clearTimeout(timer.current)
        }
    }, [search])

    const renderSales = (sale) => {
        const buyer = sale.buyer;
        const order_product = sale.products[0];
        const product = order_product.product;
        const delivery_method = sale.delivery_method;
        const payment_method = sale.payment_method;

        const date = format(parseJSON(sale.created_at), 'MM/dd/yyyy H:mm:ss');

        let status = 'Pendiente';
        switch (sale.status) {
            case PurchaseOrder.STATUS_CANCELED:
                status = 'Cancelado';
                break;
            case PurchaseOrder.STATUS_PAID:
                status = 'Pagado (Pendiente de envio)';
                break;
            case PurchaseOrder.STATUS_COMPLETED:
                status = 'Completo';
                break;
            case PurchaseOrder.STATUS_EXPIRED:
                status = 'Expirado';
                break;
            case PurchaseOrder.STATUS_RETURNED:
                status = 'Devuelto';
                break;
        }

        return (
            <IonCard key={sale.id}>
                {
                    product.photo &&
                    <IonImg src={`${config.url}/images/medium/${product.photo}`} />
                }
                <IonCardHeader>
                    <IonCardSubtitle>{product.category.name}</IonCardSubtitle>
                    <IonCardTitle>{product.name}</IonCardTitle>
                </IonCardHeader>
                <IonItem>
                    <IonLabel>Estado: {status}</IonLabel>
                </IonItem>
                <IonItem>
                    <IonLabel>Cantidad: {order_product.quantity}</IonLabel>
                </IonItem>
                <IonItem>
                    <IonLabel>Precio: $ {Intl.NumberFormat('es', { minimumFractionDigits: 2 }).format(sale.products_amount)}</IonLabel>
                </IonItem>
                {
                    sale.shipping_amount > 0 && (
                        <IonItem>
                            <IonLabel>Costo de envio: $ {Intl.NumberFormat('es', { minimumFractionDigits: 2 }).format(sale.shipping_amount)}</IonLabel>
                        </IonItem>
                    )
                }
                <IonItem>
                    <IonLabel><strong>Total: $ {Intl.NumberFormat('es', { minimumFractionDigits: 2 }).format(sale.amount)}</strong></IonLabel>
                </IonItem>
                {
                    payment_method && <IonItem>
                        <IonLabel>Pago: {payment_method.name}</IonLabel>
                    </IonItem>
                }
                {
                    delivery_method && <IonItem>
                        <IonLabel>Entrega: {delivery_method.name}</IonLabel>
                    </IonItem>
                }
                <IonItem>
                    <IonLabel>Fecha: {date}</IonLabel>
                </IonItem>
                {
                    sale.review &&
                    <IonItem>
                        <IonLabel>Calificacion: {sale.review.star}</IonLabel>
                    </IonItem>
                }
                <IonCardContent>
                    {product.detail}
                </IonCardContent>
                <IonButton onClick={e => history.push(`sales/${sale.id}`, { sale })} expand="block">Ver</IonButton>
            </IonCard>
        )
    }

    return (
        <IonPage>
            <Title>Mis Ventas</Title>
            <IonToolbar>
                <IonButtons slot="start">
                    <IonBackButton defaultHref="/login" />
                </IonButtons>
                <IonTitle>Mis Ventas</IonTitle>
            </IonToolbar>
            <IonContent>
                <IonRefresher slot="fixed" onIonRefresh={() => loadSales(1, true)} ref={refresh}>
                    <IonRefresherContent></IonRefresherContent>
                </IonRefresher>
                <IonItem>
                    <IonSearchbar onIonChange={e => setSearch(e.detail.value)} placeholder="Buscar articulos vendidos"></IonSearchbar>
                </IonItem>
                <IonItem>
                    <IonLabel>Articulos Vendidos: {meta.total ?? '-'}</IonLabel>
                </IonItem>
                {
                    submitting
                        ? <IonSpinner style={{ 'margin': 'calc(50% - 14px)' }} name="crescent" />
                        : sales.map(sale => renderSales(sale))
                }
                <IonInfiniteScroll
                    threshold="100px"
                    onIonInfinite={() => moreSales()}
                    disabled={!more}
                    ref={infiniteScroll}
                >
                <IonInfiniteScrollContent
                    loadingSpinner="crescent"
                    loadingText="Cargando..." />
                </IonInfiniteScroll>
            </IonContent>
        </IonPage>
    )
}