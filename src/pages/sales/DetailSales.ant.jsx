import React, { useState, useEffect } from 'react'
import { useHistory, useParams } from "react-router-dom"
import { IonContent,
     IonPage,
     IonTitle,
     IonToolbar,
     IonBackButton,
     IonButton,
     IonButtons,
     IonItem,
     IonLabel,
     IonImg,
     IonCardContent,
     IonCard,
     IonCardHeader,
     IonCardSubtitle,
     IonCardTitle,
} from '@ionic/react'

import { parseJSON, format } from 'date-fns'

import { PurchaseOrderService } from 'services'

import { Title } from 'components'

import config from 'config'

export const DetailSales = () => {

    const history = useHistory()
    const params = useParams()
    
    const [errors, setErrors] = useState({})
    const [showAlertError, setShowAlertError] = useState(false);
    const [submitting, setSubmitting] = useState(false)
   
    const [sale, setSale] = useState(undefined)   

    const loadSale = async () => {
        try {
            let sale = await PurchaseOrderService.detail(params.id)
            setSale(sale)
        } catch (error) {
            console.error(error)
        }
    }

    useEffect( () => {
        setSale(history.location.state.sale)
        loadSale()
    }, [])

    const renderProduct = (sale) => {
        const order_product = sale.products[0];
        const product = order_product.product;
        const delivery_method = sale.delivery_method;
        const payment_method = sale.payment_method;
        
        return (
            <IonCard key={product.id}>
                {
                    product.photo && 
                    <IonImg src={`${config.url}/images/medium/${product.photo}`} />
                }
                <IonCardHeader>
                    <IonCardSubtitle>{product.category.name}</IonCardSubtitle>
                    <IonCardTitle>{product.name}</IonCardTitle>
                </IonCardHeader>
                <IonItem>
                    <IonLabel>Precio Unitario: $ {Intl.NumberFormat('es', { minimumFractionDigits: 2 }).format(order_product.unit_price)}</IonLabel>
                </IonItem>
                <IonItem>
                    <IonLabel>Cantidad: {order_product.quantity || 0}</IonLabel>
                </IonItem>
                <IonItem>
                    <IonLabel>Sub-Total: $ {Intl.NumberFormat('es', { minimumFractionDigits: 2 }).format(sale.products_amount)}</IonLabel>
                </IonItem>
                {
                    sale.shipping_amount > 0 &&
                    <IonItem>
                        <IonLabel>Costo de envio: $ {Intl.NumberFormat('es', { minimumFractionDigits: 2 }).format(sale.shipping_amount)}</IonLabel>
                    </IonItem>
                }
                <IonItem>
                    <IonLabel><strong>Total: $ {Intl.NumberFormat('es', { minimumFractionDigits: 2 }).format(sale.amount)}</strong></IonLabel>
                </IonItem>
                <IonCardContent>
                    {product.detail}
                </IonCardContent>
                <IonButton onClick={ e => history.push(`/products/${product.id}`, { product })} expand="block">Ver</IonButton>
            </IonCard>
        )
    }
    
    return (
        <IonPage>
            <Title>{sale?.products[0]?.product?.name || ''}</Title>
            <IonToolbar>
                <IonButtons slot="start">
                    <IonBackButton defaultHref="/sales" />
                </IonButtons>
                <IonTitle>{sale?.products[0]?.product?.name || ''}</IonTitle>
            </IonToolbar>
            { 
                sale && 
                <IonContent>
                    <IonItem>
                        <IonLabel>Fecha: {format(parseJSON(sale.created_at), 'MM/dd/yyyy H:mm:ss')}</IonLabel>
                    </IonItem> 
                    <IonItem>
                        <IonLabel>Datos de Comprador</IonLabel>
                    </IonItem>
                    <IonItem>
                        <IonLabel>{sale.buyer.first_name} {sale.buyer.last_name}</IonLabel>
                    </IonItem>
                    <IonItem>
                        <IonLabel>Email: {sale.buyer.email}</IonLabel>
                    </IonItem> 
                    <IonItem>
                        <IonLabel>Pais: {sale.buyer.country_name}</IonLabel>
                    </IonItem> 
                    <IonItem>
                        <IonLabel>Provincia: {sale.buyer.state_name}</IonLabel>
                    </IonItem> 
                    <IonItem>
                        <IonLabel>Ciudad: {sale.buyer.city_name}</IonLabel>
                    </IonItem> 
                    <IonItem>
                        <IonLabel>Telefono: {sale.buyer.phone}</IonLabel>
                    </IonItem> 
                    <IonItem>
                        <IonLabel>Nomre de Calle: {sale.buyer.street_name}</IonLabel>
                    </IonItem> 
                    {
                        sale.buyer.between_streets &&
                        <IonItem>
                            <IonLabel>Entre calles: {sale.buyer.between_streets}</IonLabel>
                        </IonItem> 
                    }
                    <IonItem>
                        <IonLabel>Numero: {sale.buyer.street_number}</IonLabel>
                    </IonItem> 
                    <IonItem>
                        <IonLabel>Codigo Postal: {sale.buyer.zip_code}</IonLabel>
                    </IonItem> 
                    {
                        sale.buyer.reference &&
                        <IonItem>
                            <IonLabel>Referencia: {sale.buyer.reference}</IonLabel>
                        </IonItem> 
                    }
                    {
                        sale.payment_method &&
                        <IonItem>
                            <IonLabel>Forma de pago: {sale.payment_method.name}</IonLabel>
                        </IonItem> 
                    } 
                    {
                        sale.delivery_method &&
                        <IonItem>
                            <IonLabel>Forma de entrega: {sale.delivery_method.name}</IonLabel>
                        </IonItem> 
                    } 
                    { renderProduct(sale) }
                </IonContent>
            }
        </IonPage>
    )
}