import React, { useState, useEffect } from 'react'
import { useHistory } from "react-router-dom"

import {
    isPlatform,
    IonContent,
    IonLoading,
    IonText,
    IonPage,
    IonTitle,
    IonToolbar,
    IonInput,
    IonItem,
    IonButton,
    IonItemDivider,
    IonAlert,
    IonIcon
} from '@ionic/react'

import { GooglePlus } from '@ionic-native/google-plus'

import { logoGoogle, logoFacebook, closeCircleOutline } from 'ionicons/icons';

import GoogleLogin from 'react-google-login';

import config from 'config'

import { useContext } from 'hooks'

import { Title } from 'components'

import { UserService, FacebookService } from 'services'

export const Login = () => {

    const history = useHistory()
    const { auth } = useContext()

    const [submitting, setSubmitting] = useState(false)
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [errors, setErrors] = useState({})

    const [showAlertError, setShowAlertError] = useState(false);
    const [errorMessage, setErrorMessage] = useState('Intentelo nuevamente mas tarde');

    const login = async () => {
        setSubmitting(true)
        try {
            await auth.login({ email, password })
            loginSuccess()
        } catch (error) {
            console.log("error", JSON.stringify(error))
            if (error?.response?.data?.errors) {

                setErrors(error?.response?.data?.errors)
            } else {
                setShowAlertError(true)
            }
            // const errors = error.response?.data?.errors;
            // if (errors?.invalid_grant)
            //     setErrorMessage('Verifique los datos de acceso y vuelva a interntarlo')
            loginFailed(error)
        }
    }

    const responseLoginWithGoogle = async (response) => {
        try {
            console.log(response);
            await auth.loginWithGoogle(response.accessToken)
            loginSuccess()
        } catch (error) {
            setErrorMessage('Intentelo nuevamente mas tarde')
            loginFailed(error)
        }
    }

    const loginWithGoogle = async () => {
        setSubmitting(true)
        try {
            let response = await GooglePlus.login({
                'webClientId': config.google_client_id,
                'offline': true
            })

            await auth.loginWithGoogle(response.accessToken)
            loginSuccess()
        } catch (error) {
            setErrorMessage('Intentelo nuevamente mas tarde')
            loginFailed(error)
        }
    }

    const loginWithFacebook = async () => {
        setSubmitting(true)
        try {
            let response = await FacebookService.login(['public_profile', 'user_friends', 'email'])
            await auth.loginWithFacebook(response.authResponse.accessToken)
            loginSuccess()
        } catch (error) {
            setErrorMessage('Intentelo nuevamente mas tarde')
            loginFailed(error)
        }
    }

    const loginSuccess = async () => {

        let user = await UserService.user()
        if (user.profile_completed === true) {
            history.replace('/')
        } else {
            history.replace('/profile', { force: true })
        }
    }

    const loginFailed = (error) => {
        console.log(error)
        setShowAlertError(true)
        setSubmitting(false)
    }

    const renderErrorMessage = (field) => {
        return errors[field] && errors[field].map((message, index) => (
            <IonText key={field + index} color="danger">
                <p style={{ display: 'flex', alignItems: 'center' }}>
                    <IonIcon icon={closeCircleOutline} style={{ fontSize: '25px', marginLeft: '20px', marginRight: '10px' }} />
                    {message}
                </p>
            </IonText>
        ))
    }

    return (
        <IonPage>
            <Title>Iniciar Sesion</Title>
            <IonToolbar>
                <IonTitle>Iniciar Sesion</IonTitle>
            </IonToolbar>
            <IonContent>
                <IonItem>
                    <IonInput
                        type="email"
                        value={email}
                        placeholder="Email"
                        onIonChange={e => setEmail(e.detail.value)}
                        disabled={submitting} />
                </IonItem>
                {renderErrorMessage('email')}
                <IonItem>
                    <IonInput
                        type="password"
                        value={password}
                        placeholder="Contraseña"
                        onIonChange={e => setPassword(e.detail.value)}
                        disabled={submitting} />
                </IonItem>
                {renderErrorMessage('password')}
                <IonButton expand="block" onClick={() => login()} color="dark" disabled={submitting}>Iniciar Sesion</IonButton>
                <IonItemDivider></IonItemDivider>
                <IonButton expand="block" onClick={() => history.push('/forgot/password')} color="dark" disabled={submitting}>¿Olvidaste tu contraseña?</IonButton>
                <IonItemDivider></IonItemDivider>

                <IonButton expand="block" onClick={() => loginWithFacebook()} disabled={submitting}>
                    <IonIcon slot="start" icon={logoFacebook} />Facebook
                </IonButton>
                {
                    !isPlatform('hybrid')
                        ? <IonButton expand="block" onClick={() => loginWithGoogle()} color="danger" disabled={submitting}>
                                <IonIcon slot="start" icon={logoGoogle} />Google
                            </IonButton>
                        : <GoogleLogin
                                clientId={config.google_client_id}
                                onRequest={() => setSubmitting(true)}
                                render={props => (
                                    <IonButton expand="block" onClick={props.onClick} color="danger" disabled={submitting}>
                                        <IonIcon slot="start" icon={logoGoogle} />Google
                                    </IonButton>
                                )}
                                buttonText="Google"
                                onSuccess={responseLoginWithGoogle}
                                onFailure={(error) => loginFailed(error)}
                                cookiePolicy={'single_host_origin'}
                            />                        
                }


                <IonItemDivider></IonItemDivider>
                <IonButton expand="block" onClick={() => history.push('/register')} color="dark" disabled={submitting}>Registrarme</IonButton>

                <IonAlert
                    isOpen={showAlertError}
                    onDidDismiss={() => setShowAlertError(false)}
                    header={'No se puedo iniciar sesion'}
                    message={errorMessage}
                    buttons={['Aceptar']}
                />

                <IonLoading
                    isOpen={submitting}
                    backdropDismiss={false}
                    keyboardClose={false}
                    message={'Espere por favor...'}
                    duration={5000}
                />
            </IonContent>
        </IonPage>
    )
}

