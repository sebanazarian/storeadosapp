import React, { useState } from 'react'
import { useHistory } from "react-router-dom"
import { 
    IonContent, 
    IonPage, 
    IonTitle, 
    IonToolbar, 
    IonBackButton, 
    IonButton, 
    IonButtons, 
    IonItem, 
    IonInput, 
    IonIcon, 
    IonText, 
    IonLoading, 
    IonAlert 
} from '@ionic/react'


import { closeCircleOutline } from 'ionicons/icons';

import { RegisterService } from 'services'

import { Title } from 'components'

export const Register = () => {

    const history = useHistory()

    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [passwordConfirmation, setPasswordConfirmation] = useState('')
    const [errors, setErrors] = useState({})

    const [showAlertError, setShowAlertError] = useState(false);
    const [submitting, setSubmitting] = useState(false)

    const register = async () => {
        setSubmitting(true)

        try {
            await RegisterService.register({
                first_name: firstName,
                last_name: lastName,
                email: email,
                password: password,
                password_confirmation: passwordConfirmation,
            })

            history.replace('/login')
        } catch (error) {
            if (error?.response?.data?.errors) {
                setErrors(error?.response?.data?.errors)
            } else {
                setShowAlertError(true)
            }
        }

        setSubmitting(false)
    }

    const renderErrorMessage = (field) => {
        return errors[field] && errors[field].map((message, index) => (
            <IonText key={field + index} color="danger">
                <p style={{display: 'flex', alignItems: 'center'}}>
                    <IonIcon icon={closeCircleOutline} style={{fontSize: '25px', marginLeft: '20px', marginRight: '10px'}}/>
                    {message}
                </p>
            </IonText>
        ))
    }

    return (
        <IonPage>            
            <Title>Crear Cuenta</Title>
            <IonToolbar>
                <IonButtons slot="start">
                    <IonBackButton defaultHref="/login" />
                </IonButtons>
                <IonTitle>Crear Cuenta</IonTitle>
            </IonToolbar>
            <IonContent>
                <IonItem>
                    <IonInput
                        type="text"
                        value={firstName}
                        placeholder="Nombre"
                        onIonChange={e => setFirstName(e.detail.value)}
                        disabled={submitting}
                        required />
                </IonItem>
                {renderErrorMessage('first_name')}
                <IonItem>
                    <IonInput
                        type="text"
                        value={lastName}
                        placeholder="Apellido"
                        onIonChange={e => setLastName(e.detail.value)}
                        disabled={submitting}
                        required />
                </IonItem>
                {renderErrorMessage('last_name')}
                <IonItem>
                    <IonInput
                        type="email"
                        value={email}
                        placeholder="Email"
                        onIonChange={e => setEmail(e.detail.value)}
                        disabled={submitting}
                        required />
                </IonItem>
                {renderErrorMessage('email')}
                <IonItem>
                    <IonInput
                        type="password"
                        value={password}
                        placeholder="Contraseña"
                        onIonChange={e => setPassword(e.detail.value)}
                        disabled={submitting}
                        required />
                </IonItem>
                {renderErrorMessage('password')}
                <IonItem>
                    <IonInput
                        type="password"
                        value={passwordConfirmation}
                        placeholder="Confirme la contraseña"
                        onIonChange={e => setPasswordConfirmation(e.detail.value)}
                        disabled={submitting}
                        required />
                </IonItem>
                {renderErrorMessage('password_confirmation')}
                <IonButton expand="block" onClick={() => register()} color="dark" disabled={submitting}>Registrarme</IonButton>
                
                <IonAlert
                    isOpen={showAlertError}
                    onDidDismiss={() => setShowAlertError(false)}
                    header={'No se puedo crear la cuenta'}
                    message={'Intente nuevamente mas tarde'}
                    buttons={['Aceptar']}
                />

                <IonLoading
                    isOpen={submitting}
                    backdropDismiss={false}
                    keyboardClose={false}
                    message={'Espere por favor...'}
                    duration={5000}
                />
            </IonContent>
        </IonPage>
    )
}