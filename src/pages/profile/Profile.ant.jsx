import React, { useState, useEffect, useRef } from 'react'
import { useHistory, useParams } from "react-router-dom"

import {
    IonContent,
    IonPage,
    IonTitle,
    IonToolbar,
    IonBackButton,
    IonButtons,
    IonItem,
    IonLabel,
    IonText,
    IonItemDivider,
    IonAlert,
    IonLoading,
    IonCard,
    IonButton,
    IonRefresher,
    IonRefresherContent,
    useIonViewWillEnter
} from '@ionic/react'

import {
    UserService,
    BankAccountService,
    StoreService,
    FacebookService
} from 'services'

import { Title } from 'components'

export const Profile = () => {

    const history = useHistory()

    const refresh = useRef()

    const [user, setUser] = useState()
    const [statistics, setStatistics] = useState({})
    const [bankAccounts, setBankAccounts] = useState([])
    const [stores, setStores] = useState([])
    const [profileCompleted, setProfileCompleted] = useState(true)
    const [completed, setCompleted] = useState(false)

    const [errors, setErrors] = useState({})
    const [showAlertError, setShowAlertError] = useState(false);
    const [submitting, setSubmitting] = useState(false)
    const [loading, setLoading] = useState(true)

    const load = async (isRefresh = false) => {
        let [
            user,
            statistics,
            bankAccounts,
            stores
        ] = await Promise.all([
            UserService.user(),
            UserService.statistics(),
            BankAccountService.list(),
            StoreService.list()
        ]);

        isRefresh && refresh.current && refresh.current.complete();

        setUser(user)
        setStatistics(statistics)
        setBankAccounts(bankAccounts)
        setStores(stores)
        setLoading(false)
    }

    const linkFacebook = async () => {
        setSubmitting(true)
        try {
            if (await FacebookService.link()) {
                setUser({ ...user, facebook_link: true })
            }
        } catch (error) {
            console.log(error);
        }
        setSubmitting(false)
    }

    const unlinkFacebook = async () => {
        setSubmitting(true)
        try {
            if (await FacebookService.unlink()) {
                setUser({ ...user, facebook_link: false })
            }
        } catch (error) {
            console.log(error);
        }
        setSubmitting(false)

    }
    const renderBank = (bankAccount) => {
        return (
            <IonCard>
                <IonItem>
                    <IonLabel>Titular: {bankAccount.name}</IonLabel>
                </IonItem>
                <IonItem>
                    <IonLabel>Banco: {bankAccount.bank_name}</IonLabel>
                </IonItem>
                <IonItem>
                    <IonLabel>CBU: {bankAccount.cbu}</IonLabel>
                </IonItem>
                {
                    bankAccount.alias &&
                    <IonItem>
                        <IonLabel>Alias: {bankAccount.alias}</IonLabel>
                    </IonItem>
                }
                <IonItem>
                    <IonLabel>Numero de Cuenta: {bankAccount.account}</IonLabel>
                </IonItem>
                <IonButton onClick={e => history.push(`/bank-accounts/${bankAccount.id}/edit`, { bankAccount })} expand="block">Editar</IonButton>
                {/*<IonButton onClick={ e => history.push(`/bank-accounts/${bankAccount.id}/delete`)} expand="block">Borrar</IonButton>*/}
            </IonCard>
        )
    }

    const renderStore = (store) => {
        return (
            <IonCard>
                <IonItem>
                    <IonLabel>Nombre: {store.name}</IonLabel>
                </IonItem>
                <IonItem>
                    <IonLabel>Pais: {store.country_name}</IonLabel>
                </IonItem>
                <IonItem>
                    <IonLabel>Provincia: {store.state_name}</IonLabel>
                </IonItem>
                <IonItem>
                    <IonLabel>Ciudad: {store.city_name}</IonLabel>
                </IonItem>
                <IonItem>
                    <IonLabel>Calle: {store.street_name}</IonLabel>
                </IonItem>
                {
                    store.street_name_alt &&
                    <IonItem>
                        <IonLabel>Calle 2: {store.street_name_alt}</IonLabel>
                    </IonItem>
                }
                <IonItem>
                    <IonLabel>Entre Calles: {store.between_streets}</IonLabel>
                </IonItem>
                <IonItem>
                    <IonLabel>Numero: {store.street_number}</IonLabel>
                </IonItem>
                <IonItem>
                    <IonLabel>Codigo Postal: {store.zip_code}</IonLabel>
                </IonItem>
                {
                    store.reference &&
                    <IonItem>
                        <IonLabel>Referencia: {store.reference}</IonLabel>
                    </IonItem>
                }
                <IonButton onClick={e => history.push(`/stores/${store.id}/edit`, { store })} expand="block">Editar</IonButton>
            </IonCard>
        )
    }

    const goHome = () => {
        setProfileCompleted(true)
        history.replace(`/`)

    }
    useEffect(() => {
        load();
    }, [])

    useIonViewWillEnter(() => {

        if (history.location?.state?.user) {
            setUser(history.location.state.user)
        }

        if (history.location?.state?.bankAccount) {
            setBankAccounts([history.location.state.bankAccount])
        }

        if (history.location?.state?.store) {
            setStores([history.location.state.store])
        }

        if (history.location?.state?.force) {
            setProfileCompleted(false)
        }
    })

    useEffect(() => {
        if (!profileCompleted && stores.length > 0 && bankAccounts.length > 0) {
            setCompleted(true)
        }
    }, [stores, bankAccounts, profileCompleted])

    return (
        <IonPage>
            <Title>Perfil</Title>
            <IonToolbar>
                {
                    profileCompleted &&
                    <IonButtons slot="start">
                        <IonBackButton defaultHref="/login" />
                    </IonButtons>
                }

                <IonTitle>Perfil</IonTitle>
            </IonToolbar>
            <IonContent>
                <IonRefresher slot="fixed" onIonRefresh={() => load(true)} ref={refresh}>
                    <IonRefresherContent></IonRefresherContent>
                </IonRefresher>
                {
                    !profileCompleted &&
                    <IonItem>
                        <IonLabel >POR FAVOR AGREGAR UN BANCO Y UNA DIRECCION</IonLabel>
                    </IonItem>
                }
                {
                    user && <>
                        <IonItem>
                            <IonLabel>Reputación: {statistics.reputation} </IonLabel>
                        </IonItem>
                        <IonItem>
                            <IonLabel>Publicaciones: {statistics.total_posts} </IonLabel>
                        </IonItem>
                        <IonItem>
                            <IonLabel>Mensajes Pendientes: {statistics.total_messages || 0} </IonLabel>
                        </IonItem>
                        <IonItem>
                            <IonLabel>Ventas: {statistics.total_sales || 0} </IonLabel>
                        </IonItem>
                        <IonItemDivider />
                        {
                            user.facebook_link
                                ? <IonButton onClick={unlinkFacebook} color="danger" expand="block">Desvincular Facebook</IonButton>
                                : <IonButton onClick={linkFacebook} color="success" expand="block">Vincular Facebook</IonButton>
                        }
                        <IonItemDivider />
                        <IonItem>
                            <IonText><strong>Datos Personales</strong></IonText>
                        </IonItem>
                        <IonCard>
                            <IonItem>
                                <IonLabel>Nombre: {user.first_name}</IonLabel>
                            </IonItem>
                            <IonItem>
                                <IonLabel>Apellido: {user.last_name}</IonLabel>
                            </IonItem>
                            <IonItem>
                                <IonLabel>Email: {user.email}</IonLabel>
                            </IonItem>
                            <IonItem>
                                <IonLabel>Telefono: {user.phone}</IonLabel>
                            </IonItem>
                            <IonButton onClick={e => history.push(`/profile/edit`, { user })} expand="block">Editar Perfil</IonButton>
                        </IonCard>
                        <IonItem>
                            <IonText><strong>Datos Bancarios</strong></IonText>
                        </IonItem>
                        {
                            bankAccounts[0]
                                ? renderBank(bankAccounts[0])
                                : <IonButton onClick={e => history.push(`/bank-accounts/create`)} expand="block">Agregar</IonButton>
                        }
                        <IonItem>
                            <IonText><strong>Datos Logisticos</strong></IonText>
                        </IonItem>
                        {
                            stores[0]
                                ? renderStore(stores[0])
                                : <IonButton onClick={e => history.push(`/stores/create`)} expand="block">Agregar</IonButton>
                        }
                        {
                            !profileCompleted &&
                            <IonButton onClick={e => goHome()} expand="block" disabled={!completed}>Continuar</IonButton>
                        }
                    </>
                }
                <IonAlert
                    isOpen={showAlertError}
                    onDidDismiss={() => setShowAlertError(false)}
                    header={'No se puedo crear la cuenta'}
                    message={'Intente nuevamente mas tarde'}
                    buttons={['Aceptar']}
                />

                <IonLoading
                    isOpen={loading}
                    backdropDismiss={false}
                    keyboardClose={false}
                    message={'Cargando...'}
                    duration={5000}
                />

                <IonLoading
                    isOpen={submitting}
                    backdropDismiss={false}
                    keyboardClose={false}
                    message={'Espere por favor...'}
                    duration={0}
                />
            </IonContent>
        </IonPage>
    )
}