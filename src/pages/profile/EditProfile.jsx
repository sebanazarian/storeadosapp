import React, { useState, useEffect } from 'react'
import { useHistory } from "react-router-dom"

import { IonLoading, IonAlert } from '@ionic/react'

import { UserService } from 'services'

import { App, Content, Spinner, Input, Button } from 'components'

export const EditProfile = () => {

    const history = useHistory()

    const [errors, setErrors] = useState({})

    const [showAlertError, setShowAlertError] = useState(false);
    const [submitting, setSubmitting] = useState(false)

    const [user, setUser] = useState()
    const [firstName, setFirstName] = useState()
    const [lastName, setLastName] = useState()
    const [phone, setPhone] = useState()
    const [valid, setValid] = useState(false)

    useEffect(() => {
        const user = history.location.state.user;
        setUser(user)
        setFirstName(user.first_name)
        setLastName(user.last_name)
        setPhone(user.phone)
    }, [])


    useEffect(() => {
        let valid = user ? (
            user.first_name != firstName ||
            user.last_name != lastName ||
            user.phone != phone) : true

        setValid(
            firstName && firstName.trim() != '' &&
            lastName && lastName.trim() != '' &&
            phone && phone.trim() != '' && valid
        )
    }, [user, firstName, lastName, phone])

    const updatePerfile = async () => {
        setSubmitting(true);

        try {
            let user = await UserService.update({
                first_name: firstName,
                last_name: lastName,
                phone: phone
            })

            history.replace('/profile', { user });
        } catch (error) {
            if (error?.response?.data?.errors) {
                setErrors(error?.response?.data?.errors)
            } else {
                setShowAlertError(true)
            }
        }

        setSubmitting(false);
    }

    return (
        <App title="Mi cuenta" titlebar onBack={() => history.goBack()}>
            <Content margin>
                <div className="row">
                    <div className="col-12 col-sm-12 col-md-12 col-lg-12">
                        <article className="wow fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.5s">
                            {!user ? <Spinner /> :
                                <div className="App-MiCuenta-DatosFormularios mt-4">
                                    <div className="App-MiCuenta-DatosFormularios-Box mb-4 wow fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.5s">
                                        <h2 className="Title FontTitle1 mb-3">Datos personales</h2>
                                        <form className="FormularioLogin FormularioBase" action="#">
                                            <Input type="text"
                                                placeholder="Nombre"
                                                value={firstName}
                                                onChange={e => setFirstName(e.target.value)}
                                                errors={errors['first_name']}
                                                disabled={submitting}
                                            />
                                            <Input type="text"
                                                placeholder="Apellido"
                                                value={lastName}
                                                onChange={e => setLastName(e.target.value)}
                                                errors={errors['last_name']}
                                                disabled={submitting}
                                            />
                                            <Input type="text"
                                                placeholder="Teléfono"
                                                value={phone}
                                                onChange={e => setPhone(e.target.value)}
                                                errors={errors['phone']}
                                                disabled={submitting}
                                            />
                                            <Button onClick={() => updatePerfile()} color="primary" disabled={!valid || submitting}>
                                                Guardar
                                            </Button>
                                        </form>
                                    </div>
                                </div>
                            }
                        </article>
                    </div>
                </div>
            </Content>
            <IonAlert
                isOpen={showAlertError}
                onDidDismiss={() => setShowAlertError(false)}
                header={'No se puedo actualizar el perfil'}
                message={'Intente nuevamente mas tarde'}
                buttons={['Aceptar']}
            />

            <IonLoading
                isOpen={submitting}
                backdropDismiss={false}
                keyboardClose={false}
                message={'Espere por favor...'}
                duration={5000}
            />
        </App>
    )
}

