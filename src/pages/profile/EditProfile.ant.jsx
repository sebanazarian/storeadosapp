import React, { useState, useEffect} from 'react'
import { useHistory } from "react-router-dom"

import { 
    IonContent,
     IonPage,
     IonTitle,
     IonToolbar,
     IonBackButton,
     IonButton,
     IonButtons,
     IonItem,
     IonInput,
     IonIcon,
     IonText,
     IonLoading,
     IonAlert
} from '@ionic/react'

import { closeCircleOutline } from 'ionicons/icons';

import { UserService } from 'services'

import { Title } from 'components'

export const EditProfile = () => {

    const history = useHistory()

    const [errors, setErrors] = useState({})

    const [showAlertError, setShowAlertError] = useState(false);
    const [submitting, setSubmitting] = useState(false)

    const [user, setUser] = useState(undefined)  
    const [firstName, setFirstName] = useState(undefined)  
    const [lastName, setLastName] = useState(undefined)   
    const [phone, setPhone] = useState(undefined)  

    useEffect(() => {
        const user = history.location.state.user;
        setUser(user)
        setFirstName(user.first_name)
        setLastName(user.last_name)
        setPhone(user.phone)
    }, [])

    const updatePerfile = async () => {
        setSubmitting(true);

        try {
            let user = await UserService.update({
                first_name: firstName,
                last_name: lastName,
                phone: phone
            })
            
            history.replace('/profile', { user });
        } catch (error) {
            if (error?.response?.data?.errors) {
                setErrors(error?.response?.data?.errors)
            } else {
                setShowAlertError(true)
            }
        }

        setSubmitting(false);
    }

    const renderErrorMessage = (field) => {
        return errors[field] && errors[field].map((message, index) => (
            <IonText key={field + index} color="danger">
                <p style={{display: 'flex', alignItems: 'center'}}>
                    <IonIcon icon={closeCircleOutline} style={{fontSize: '25px', marginLeft: '20px', marginRight: '10px'}}/>
                    {message}
                </p>
            </IonText>
        ))
    }

    return (
        <IonPage>          
            <Title>Datos Personales</Title>
            <IonToolbar>
                <IonButtons slot="start">
                    <IonBackButton defaultHref="/login" />
                </IonButtons>
                <IonTitle>Datos Personales</IonTitle>
            </IonToolbar>
            <IonContent>
                {
                    user && <>
                        <IonItem>
                            <IonInput
                                type="text"
                                value={firstName}
                                placeholder="Nombre"
                                onIonChange={e => setFirstName(e.detail.value)}
                                disabled={submitting}
                                required />
                        </IonItem>
                        {renderErrorMessage('first_name')}
                        <IonItem>
                            <IonInput
                                type="text"
                                value={lastName}
                                placeholder="Apellido"
                                onIonChange={e => setLastName(e.detail.value)}
                                disabled={submitting}
                                required />
                        </IonItem>
                        {renderErrorMessage('last_name')}
                        <IonItem>
                            <IonInput
                                type="text"
                                value={phone}
                                placeholder="Telefono"
                                onIonChange={e => setPhone(e.detail.value)}
                                disabled={submitting}
                                required />
                        </IonItem>
                        {renderErrorMessage('phone')}
                        <IonButton expand="block"  color="dark" onClick={ () => updatePerfile() } disabled={submitting} expand="block">Guardar</IonButton>
                        
                        <IonAlert
                            isOpen={showAlertError}
                            onDidDismiss={() => setShowAlertError(false)}
                            header={'No se puedo actualizar el perfil'}
                            message={'Intente nuevamente mas tarde'}
                            buttons={['Aceptar']}
                        />

                        <IonLoading
                            isOpen={submitting}
                            backdropDismiss={false}
                            keyboardClose={false}
                            message={'Espere por favor...'}
                            duration={5000}
                        />
                    </>
                }
            </IonContent>
        </IonPage>
    )
}

