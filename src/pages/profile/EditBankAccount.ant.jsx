import React, { useState, useEffect} from 'react'
import { useHistory, useParams} from "react-router-dom"

import { 
    IonContent,
    IonPage,
    IonTitle,
    IonToolbar,
    IonBackButton,
    IonButton,
    IonButtons,
    IonItem,
    IonInput,
    IonIcon,
    IonText,
    IonLoading,
    IonAlert
} from '@ionic/react'

import { closeCircleOutline } from 'ionicons/icons';

import { BankAccountService } from 'services'

import { Title } from 'components'

export const EditBankAccount = () => {

    const history = useHistory()
    const params = useParams()

    const [errors, setErrors] = useState({})

    const [showAlertError, setShowAlertError] = useState(false);
    const [submitting, setSubmitting] = useState(false)

    const [bankAccount, setBankAccount] = useState(undefined)
    const [name, setName] = useState(undefined)
    const [bankName, setBankName] = useState(undefined)
    const [account, setAccount] = useState(undefined)
    const [cbu, setCBU] = useState(undefined) 
    const [alias, setAlias] = useState(undefined) 

    useEffect(() => {
        const bankAccount = history.location.state.bankAccount;

        if(bankAccount) {
            setBankAccount(bankAccount)
            setName(bankAccount.name)
            setBankName(bankAccount.bank_name)
            setAccount(bankAccount.account)
            setCBU(bankAccount.cbu)
            setAlias(bankAccount.alias)
        }
    }, [])

    const createBanckAccount = async () => {
        setSubmitting(true);

        try {
            let data = {
                name,
                account,
                cbu,
                alias,
                bank_name: bankName
            }
            
            let bankAccount = await (params.id 
                ? BankAccountService.update(params.id, data)
                : BankAccountService.create(data))
            
            history.replace('/profile', { bankAccount });
        } catch (error) {
            console.log(error);
            if (error?.response?.data?.errors) {
                setErrors(error?.response?.data?.errors)
            } else {
                setShowAlertError(true)
            }
        }

        setSubmitting(false);
    }

    const renderErrorMessage = (field) => {
        return errors[field] && errors[field].map((message, index) => (
            <IonText key={field + index} color="danger">
                <p style={{display: 'flex', alignItems: 'center'}}>
                    <IonIcon icon={closeCircleOutline} style={{fontSize: '25px', marginLeft: '20px', marginRight: '10px'}}/>
                    {message}
                </p>
            </IonText>
        ))
    }

    return (
        <IonPage>
            <Title>Datos Bancarios</Title>
            <IonToolbar>
                <IonButtons slot="start">
                    <IonBackButton defaultHref="/login" />
                </IonButtons>
                <IonTitle>Datos Bancarios</IonTitle>
            </IonToolbar>
            <IonContent>
                <IonItem>
                    <IonInput
                        type="text"
                        value={name}
                        placeholder="Nombre"
                        onIonChange={e => setName(e.detail.value)}
                        disabled={submitting}
                        required />
                </IonItem>
                {renderErrorMessage('name')}
                <IonItem>
                    <IonInput
                        type="text"
                        value={bankName}
                        placeholder="Nombre del Banco"
                        onIonChange={e => setBankName(e.detail.value)}
                        disabled={submitting}
                        required />
                </IonItem>
                {renderErrorMessage('bank_name')}
                <IonItem>
                    <IonInput
                        type="text"
                        value={account}
                        placeholder="Numero de Cuenta"
                        onIonChange={e => setAccount(e.detail.value)}
                        disabled={submitting}
                        required />
                </IonItem>
                {renderErrorMessage('account')}
                <IonItem>
                    <IonInput
                        type="text"
                        value={cbu}
                        placeholder="CBU"
                        onIonChange={e => setCBU(e.detail.value)}
                        disabled={submitting}
                        required />
                </IonItem>
                {renderErrorMessage('cbu')}
                <IonItem>
                    <IonInput
                        type="text"
                        value={alias}
                        placeholder="Alias"
                        onIonChange={e => setAlias(e.detail.value)}
                        disabled={submitting}
                        required />
                </IonItem>
                {renderErrorMessage('alias')}
                <IonButton expand="block"  color="dark" onClick={ () => createBanckAccount() } disabled={submitting} expand="block">Guardar</IonButton>
                
                <IonAlert
                    isOpen={showAlertError}
                    onDidDismiss={() => setShowAlertError(false)}
                    header={'No se puedo actualizar el perfil'}
                    message={'Intente nuevamente mas tarde'}
                    buttons={['Aceptar']}
                />

                <IonLoading
                    isOpen={submitting}
                    backdropDismiss={false}
                    keyboardClose={false}
                    message={'Espere por favor...'}
                    duration={5000}
                />
            </IonContent>
        </IonPage>
    )
}

