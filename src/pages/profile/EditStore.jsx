import React, { useState, useEffect } from 'react'
import { useHistory, useParams } from "react-router-dom"

import { IonLoading, IonAlert } from '@ionic/react'

import { StoreService } from 'services'

import { App, Content, Input, Spinner, Button, Image } from 'components'

export const EditStore = () => {

    const history = useHistory()
    const params = useParams()

    const [errors, setErrors] = useState({})

    const [showAlertError, setShowAlertError] = useState(false);
    const [submitting, setSubmitting] = useState(false)

    const [store, setStore] = useState()
    const [name, setName] = useState()
    const [countryName, setCountryName] = useState()
    const [stateName, setStateName] = useState()
    const [cityName, setCityName] = useState()
    const [streetName, setStreetName] = useState()
    const [streetNameAlt, setStreetNameAlt] = useState()
    const [betweenStreets, setBetweenStreets] = useState()
    const [streetNumber, setStreetNumber] = useState()
    const [zipCode, setZipCode] = useState()
    const [reference, setReference] = useState()
    const [valid, setValid] = useState(false)

    useEffect(() => {
        const { store, create } = history.location?.state;

        if (!store && !create) {
            history.replace('/profile')
            return;
        }

        if (store) {
            setStore(store)
            setName(store.name);
            setCountryName(store.country_name);
            setStateName(store.state_name);
            setCityName(store.city_name);
            setStreetName(store.street_name);
            setStreetNameAlt(store.street_name_alt);
            setBetweenStreets(store.between_streets);
            setStreetNumber(store.street_number);
            setZipCode(store.zip_code);
            setReference(store.reference);
        }
    }, [])


    useEffect(() => {
        let valid = store ? (store.name != name ||
                    store.country_name != countryName ||
                    store.state_name != stateName ||
                    store.city_name != cityName ||
                    store.street_name != streetName ||
                    store.street_name_alt != streetNameAlt ||
                    store.between_streets != betweenStreets ||
                    store.street_number != streetNumber ||
                    store.zip_code != zipCode ||
                    store.reference != reference) : true

        setValid(
            name && name.trim() != '' &&
            countryName && countryName.trim() != '' &&
            stateName && stateName.trim() != '' &&
            cityName && cityName.trim() != '' &&
            streetName && streetName.trim() != '' &&
            //streetNameAlt && streetNameAlt.trim() != '' &&
            betweenStreets && betweenStreets.trim() != '' &&
            streetNumber && streetNumber > 0 &&
            zipCode && zipCode > 0 &&
            reference && reference.trim() != '' && valid
        )
    }, [name, countryName, stateName, cityName, streetName, streetNameAlt, betweenStreets, streetNumber, zipCode, reference])

    const createStore = async () => {
        setSubmitting(true);

        try {
            let data = {
                name,
                country_name: countryName,
                state_name: stateName,
                city_name: cityName,
                street_name: streetName,
                street_name_alt: streetNameAlt,
                between_streets: betweenStreets,
                street_number: streetNumber,
                zip_code: zipCode,
                reference
            }

            let store = await (params.id
                ? StoreService.update(params.id, data)
                : StoreService.create(data))

            history.replace('/profile', { store });
        } catch (error) {
            console.log(error);
            if (error?.response?.data?.errors) {
                setErrors(error?.response?.data?.errors)
            } else {
                setShowAlertError(true)
            }
        }

        setSubmitting(false);
    }

    return (
        <App title="Datos Logisticos" titlebar onBack={() => history.goBack()}>
            <Content margin>
                <div className="row">
                    <div className="col-12 col-sm-12 col-md-12 col-lg-12">
                        <article className="wow fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.5s">
                            <div className="App-MiCuenta-DatosFormularios mt-4">
                                <div className="App-MiCuenta-DatosFormularios-Box mb-4 wow fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.5s">
                                    <h2 className="Title FontTitle1 mb-3">Datos personales</h2>
                                    <form className="FormularioLogin FormularioBase" action="#">
                                        <Input
                                            type="text"
                                            value={name}
                                            placeholder="Nombre"
                                            onChange={e => setName(e.target.value)}
                                            disabled={submitting}
                                            errors={errors['name']}
                                        />
                                        <Input
                                            type="text"
                                            value={countryName}
                                            placeholder="Pais"
                                            onChange={e => setCountryName(e.target.value)}
                                            disabled={submitting}
                                            errors={errors['country_name']}
                                        />
                                        <Input
                                            type="text"
                                            value={stateName}
                                            placeholder="Provincia"
                                            onChange={e => setStateName(e.target.value)}
                                            disabled={submitting}
                                            errors={errors['state_name']}
                                        />
                                        <Input
                                            type="text"
                                            value={cityName}
                                            placeholder="Ciudad"
                                            onChange={e => setCityName(e.target.value)}
                                            disabled={submitting}
                                            errors={errors['city_name']}
                                        />
                                        <Input
                                            type="text"
                                            value={streetName}
                                            placeholder="Calle 1"
                                            onChange={e => setStreetName(e.target.value)}
                                            disabled={submitting}
                                            errors={errors['street_name']}
                                        />
                                        <Input
                                            type="text"
                                            value={streetNameAlt}
                                            placeholder="Calle 2"
                                            onChange={e => setStreetNameAlt(e.target.value)}
                                            disabled={submitting}
                                            errors={errors['street_name_alt']}
                                        />
                                        <Input
                                            type="text"
                                            value={betweenStreets}
                                            placeholder="Entre Calles"
                                            onChange={e => setBetweenStreets(e.target.value)}
                                            disabled={submitting}
                                            errors={errors['between_streets']}
                                        />
                                        <Input
                                            type="text"
                                            value={streetNumber}
                                            placeholder="Numero"
                                            onChange={e => setStreetNumber(e.target.value)}
                                            disabled={submitting}
                                            errors={errors['street_number']}
                                        />
                                        <Input
                                            type="text"
                                            value={zipCode}
                                            placeholder="Codigo Postal"
                                            onChange={e => setZipCode(e.target.value)}
                                            disabled={submitting}
                                            errors={errors['zip_code']}
                                        />
                                        <Input
                                            type="text"
                                            value={reference}
                                            placeholder="Referencia"
                                            onChange={e => setReference(e.target.value)}
                                            disabled={submitting}
                                            errors={errors['reference']}
                                        />

                                        <Button onClick={() => createStore()} color="primary" disabled={!valid || submitting}>
                                            Guardar
                                        </Button>
                                    </form>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </Content>
            <IonAlert
                isOpen={showAlertError}
                onDidDismiss={() => setShowAlertError(false)}
                header={'No se puedo actualizar el perfil'}
                message={'Intente nuevamente mas tarde'}
                buttons={['Aceptar']}
            />

            <IonLoading
                isOpen={submitting}
                backdropDismiss={false}
                keyboardClose={false}
                message={'Espere por favor...'}
                duration={5000}
            />
        </App>
    )
}

