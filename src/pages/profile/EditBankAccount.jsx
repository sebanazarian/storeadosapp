import React, { useState, useEffect } from 'react'
import { useHistory, useParams } from "react-router-dom"

import {
    IonContent,
    IonPage,
    IonTitle,
    IonToolbar,
    IonBackButton,
    IonButton,
    IonButtons,
    IonItem,
    IonInput,
    IonIcon,
    IonText,
    IonLoading,
    IonAlert
} from '@ionic/react'

import { closeCircleOutline } from 'ionicons/icons';

import { BankAccountService } from 'services'

import { App, Content, Input, Spinner, Button, Image } from 'components'

export const EditBankAccount = () => {

    const history = useHistory()
    const params = useParams()

    const [errors, setErrors] = useState({})

    const [showAlertError, setShowAlertError] = useState(false);
    const [submitting, setSubmitting] = useState(false)

    const [bankAccount, setBankAccount] = useState()
    const [name, setName] = useState()
    const [bankName, setBankName] = useState()
    const [account, setAccount] = useState()
    const [cbu, setCBU] = useState()
    const [alias, setAlias] = useState()
    const [valid, setValid] = useState(false)

    useEffect(() => {
        const { bankAccount, create } = history.location?.state;

        if (!bankAccount && !create) {
            history.replace('/profile')
            return;
        }

        if (bankAccount) {
            setBankAccount(bankAccount)
            setName(bankAccount.name)
            setBankName(bankAccount.bank_name)
            setAccount(bankAccount.account)
            setCBU(bankAccount.cbu)
            setAlias(bankAccount.alias)
        }
    }, [])

    useEffect(() => {
        let valid = bankAccount ? (
            bankAccount.name != name ||
            bankAccount.bank_name != bankName ||
            bankAccount.account != account ||
            bankAccount.alias != alias ||
            bankAccount.cbu != cbu) : true

        setValid(
            name && name.trim() != '' &&
            bankName && bankName.trim() != '' &&
            account && account.trim() != '' &&
            cbu && cbu.trim() != '' && valid
        )
    }, [name, bankName, account, cbu, alias])

    const createBanckAccount = async () => {
        setSubmitting(true);

        try {
            let data = {
                name,
                account,
                cbu,
                alias,
                bank_name: bankName
            }

            let bankAccount = await (params.id
                ? BankAccountService.update(params.id, data)
                : BankAccountService.create(data))

            history.replace('/profile', { bankAccount });
        } catch (error) {
            console.log(error);
            if (error?.response?.data?.errors) {
                setErrors(error?.response?.data?.errors)
            } else {
                setShowAlertError(true)
            }
        }

        setSubmitting(false);
    }

    return (
        <App title="Datos Bancarios" titlebar onBack={() => history.goBack()}>
            <Content margin>
                <div className="row">
                    <div className="col-12 col-sm-12 col-md-12 col-lg-12">
                        <article className="wow fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.5s">
                            <div className="App-MiCuenta-DatosFormularios mt-4">
                                <div className="App-MiCuenta-DatosFormularios-Box mb-4 wow fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.5s">
                                    <h2 className="Title FontTitle1 mb-3">Datos Bancarios</h2>
                                    <form className="FormularioLogin FormularioBase" action="#">
                                        <Input
                                            type="text"
                                            value={name}
                                            placeholder="Nombre"
                                            onChange={e => setName(e.target.value)}
                                            disabled={submitting}
                                            errors={errors['name']} />
                                        <Input
                                            type="text"
                                            value={bankName}
                                            placeholder="Nombre del Banco"
                                            onChange={e => setBankName(e.target.value)}
                                            disabled={submitting}
                                            errors={errors['bank_name']} />
                                        <Input
                                            type="text"
                                            value={account}
                                            placeholder="Numero de Cuenta"
                                            onChange={e => setAccount(e.target.value)}
                                            disabled={submitting}
                                            errors={errors['account']} />
                                        <Input
                                            type="text"
                                            value={cbu}
                                            placeholder="CBU"
                                            onChange={e => setCBU(e.target.value)}
                                            disabled={submitting}
                                            errors={errors['cbu']} />
                                        <Input
                                            type="text"
                                            value={alias}
                                            placeholder="Alias"
                                            onChange={e => setAlias(e.target.value)}
                                            disabled={submitting}
                                            errors={errors['alias']} />
                                        <Button onClick={() => createBanckAccount()} color="primary" disabled={!valid || submitting}>
                                            Guardar
                                        </Button>
                                    </form>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </Content>
            <IonAlert
                isOpen={showAlertError}
                onDidDismiss={() => setShowAlertError(false)}
                header={'No se puedo actualizar el perfil'}
                message={'Intente nuevamente mas tarde'}
                buttons={['Aceptar']}
            />

            <IonLoading
                isOpen={submitting}
                backdropDismiss={false}
                keyboardClose={false}
                message={'Espere por favor...'}
                duration={5000}
            />
        </App>

    )
}

