import React, { useState, useEffect} from 'react'
import { useHistory, useParams} from "react-router-dom"

import { 
    IonContent,
     IonPage,
     IonTitle,
     IonToolbar,
     IonBackButton,
     IonButton,
     IonButtons,
     IonItem,
     IonInput,
     IonIcon,
     IonText,
     IonLoading,
     IonAlert
} from '@ionic/react'

import { closeCircleOutline } from 'ionicons/icons';

import { StoreService } from 'services'

import { Title } from 'components'

export const EditStore = () => {

    const history = useHistory()
    const params = useParams()

    const [errors, setErrors] = useState({})

    const [showAlertError, setShowAlertError] = useState(false);
    const [submitting, setSubmitting] = useState(false)

    const [store, setStore] = useState(undefined)
    const [name, setName] = useState(undefined)
    const [countryName, setCountryName] = useState(undefined)
    const [stateName, setStateName] = useState(undefined)
    const [cityName, setCityName] = useState(undefined)
    const [streetName, setStreetName] = useState(undefined)
    const [streetNameAlt, setStreetNameAlt] = useState(undefined)
    const [betweenStreets, setBetweenStreets] = useState(undefined)
    const [streetNumber, setStreetNumber] = useState(undefined)
    const [zipCode, setZipCode] = useState(undefined)
    const [reference, setReference] = useState(undefined)


    useEffect(() => {
        const store = history.location.state.store;

        if(store) {
            setStore(store)
            setName(store.name);
            setCountryName(store.country_name);
            setStateName(store.state_name);
            setCityName(store.city_name);
            setStreetName(store.street_name);
            setStreetNameAlt(store.street_name_alt);
            setBetweenStreets(store.between_streets);
            setStreetNumber(store.street_number);
            setZipCode(store.zip_code);
            setReference(store.reference);
        }
    }, [])

    const createStore = async () => {
        setSubmitting(true);

        try {
            let data = {
                name,
                country_name: countryName,
                state_name: stateName,
                city_name: cityName,
                street_name: streetName,
                street_name_alt: streetNameAlt,
                between_streets: betweenStreets,
                street_number: streetNumber,
                zip_code: zipCode,
                reference
            }
            
            let store = await (params.id 
                ? StoreService.update(params.id, data)
                : StoreService.create(data))
            
            history.replace('/profile', { store });
        } catch (error) {
            console.log(error);
            if (error?.response?.data?.errors) {
                setErrors(error?.response?.data?.errors)
            } else {
                setShowAlertError(true)
            }
        }

        setSubmitting(false);
    }

    const renderErrorMessage = (field) => {
        return errors[field] && errors[field].map((message, index) => (
            <IonText key={field + index} color="danger">
                <p style={{display: 'flex', alignItems: 'center'}}>
                    <IonIcon icon={closeCircleOutline} style={{fontSize: '25px', marginLeft: '20px', marginRight: '10px'}}/>
                    {message}
                </p>
            </IonText>
        ))
    }

    return (
        <IonPage>
            <Title>Datos Logisticos</Title>
            <IonToolbar>
                <IonButtons slot="start">
                    <IonBackButton defaultHref="/login" />
                </IonButtons>
                <IonTitle>Datos Logisticos</IonTitle>
            </IonToolbar>
            <IonContent>
                <IonItem>
                    <IonInput
                        type="text"
                        value={name}
                        placeholder="Nombre"
                        onIonChange={e => setName(e.detail.value)}
                        disabled={submitting}
                        required />
                </IonItem>
                {renderErrorMessage('name')}
                <IonItem>
                    <IonInput
                        type="text"
                        value={countryName}
                        placeholder="Pais"
                        onIonChange={e => setCountryName(e.detail.value)}
                        disabled={submitting}
                        required />
                </IonItem>
                {renderErrorMessage('country_name')}
                <IonItem>
                    <IonInput
                        type="text"
                        value={stateName}
                        placeholder="Provincia"
                        onIonChange={e => setStateName(e.detail.value)}
                        disabled={submitting}
                        required />
                </IonItem>
                {renderErrorMessage('state_name')}
                <IonItem>
                    <IonInput
                        type="text"
                        value={cityName}
                        placeholder="Ciudad"
                        onIonChange={e => setCityName(e.detail.value)}
                        disabled={submitting}
                        required />
                </IonItem>
                {renderErrorMessage('city_name')}
                <IonItem>
                    <IonInput
                        type="text"
                        value={streetName}
                        placeholder="Calle 1"
                        onIonChange={e => setStreetName(e.detail.value)}
                        disabled={submitting}
                        required />
                </IonItem>
                {renderErrorMessage('street_name')}
                <IonItem>
                    <IonInput
                        type="text"
                        value={streetNameAlt}
                        placeholder="Calle 2"
                        onIonChange={e => setStreetNameAlt(e.detail.value)}
                        disabled={submitting}
                        required />
                </IonItem>
                {renderErrorMessage('street_name_alt')}                
                <IonItem>
                    <IonInput
                        type="text"
                        value={betweenStreets}
                        placeholder="Entre Calles"
                        onIonChange={e => setBetweenStreets(e.detail.value)}
                        disabled={submitting}
                        required />
                </IonItem>
                {renderErrorMessage('between_streets')}
                <IonItem>
                    <IonInput
                        type="text"
                        value={streetNumber}
                        placeholder="Numero"
                        onIonChange={e => setStreetNumber(e.detail.value)}
                        disabled={submitting}
                        required />
                </IonItem>
                {renderErrorMessage('street_number')}
                <IonItem>
                    <IonInput
                        type="text"
                        value={zipCode}
                        placeholder="Codigo Postal"
                        onIonChange={e => setZipCode(e.detail.value)}
                        disabled={submitting}
                        required />
                </IonItem>
                {renderErrorMessage('zip_code')}
                 <IonItem>
                    <IonInput
                        type="text"
                        value={reference}
                        placeholder="Referencia"
                        onIonChange={e => setReference(e.detail.value)}
                        disabled={submitting}
                        required />
                </IonItem>
                {renderErrorMessage('reference')}

                <IonButton expand="block"  color="dark" onClick={ () => createStore() } disabled={submitting} expand="block">Guardar</IonButton>
                
                <IonAlert
                    isOpen={showAlertError}
                    onDidDismiss={() => setShowAlertError(false)}
                    header={'No se puedo actualizar el perfil'}
                    message={'Intente nuevamente mas tarde'}
                    buttons={['Aceptar']}
                />

                <IonLoading
                    isOpen={submitting}
                    backdropDismiss={false}
                    keyboardClose={false}
                    message={'Espere por favor...'}
                    duration={5000}
                />
            </IonContent>
        </IonPage>
    )
}

