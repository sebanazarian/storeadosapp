import React, { useState, useEffect } from 'react'
import { useHistory } from "react-router-dom"
import { closeCircleOutline } from 'ionicons/icons';

import {
    IonContent,
    IonPage,
    IonTitle,
    IonToolbar,
    IonItem,
    IonInput,
    IonButton,
    IonLoading,
    IonAlert,
    IonText,
    IonIcon,
    IonBackButton,
    IonButtons
} from '@ionic/react'

import { AuthService } from 'services'

import { Title } from 'components'

export const ForgotPassword = () => {

    const history = useHistory()

    const [email, setEmail] = useState()
    const [errors, setErrors] = useState({})

    const [success, setSuccess] = useState(false);
    const [showAlertError, setShowAlertError] = useState(false);
    const [submitting, setSubmitting] = useState(false)

    const forgotPassword = async (email) => {
        setErrors([])
        setSubmitting(true)
        try {
            await AuthService.forgotPassword(email)
            setSuccess(true)
        } catch (error) {
            if (error?.response?.data?.errors) {
                setErrors(error?.response?.data?.errors)
            } else {
                setShowAlertError(true)
            }
        }
        setSubmitting(false)
    }

    const renderErrorMessage = (field) => {
        return errors[field] && errors[field].map((message, index) => (
            <IonText key={field + index} color="danger">
                <p style={{ display: 'flex', alignItems: 'center' }}>
                    <IonIcon icon={closeCircleOutline} style={{ fontSize: '25px', marginLeft: '20px', marginRight: '10px' }} />
                    {message}
                </p>
            </IonText>
        ))
    }

    return (
        <IonPage>
            <Title/>
            <IonToolbar>
                <IonButtons slot="start">
                    <IonBackButton defaultHref="/login" />
                </IonButtons>
                <IonTitle>Ayuda con la contraseña</IonTitle>
            </IonToolbar>
            <IonContent className="center">
                <IonItem>
                    <IonInput
                        type="email"
                        value={email}
                        placeholder="Email"
                        onIonChange={e => setEmail(e.detail.value)}
                        disabled={submitting}
                        required />
                </IonItem>
                {renderErrorMessage('email')}

                <IonButton expand="block" onClick={() => forgotPassword(email)} color="dark" disabled={submitting}>Continuar</IonButton>

                <IonAlert
                    isOpen={success}
                    onDidDismiss={() => history.goBack()}
                    header={'Mensaje Enviado'}
                    message={'¡Recordatorio de contraseña enviado!'}
                    buttons={['Aceptar']}
                />
                
                <IonAlert
                    isOpen={showAlertError}
                    onDidDismiss={() => setShowAlertError(false)}
                    header={'No se puedo crear la cuenta'}
                    message={'Intente nuevamente mas tarde'}
                    buttons={['Aceptar']}
                />

            </IonContent>
        </IonPage>
    )
}