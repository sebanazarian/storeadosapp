export const STATUS_PENDING = 'pending';
export const STATUS_CANCELED = 'canceled';
export const STATUS_PAID = 'paid';
export const STATUS_COMPLETED = 'completed';
export const STATUS_EXPIRED  = 'expired';
export const STATUS_RETURNED  = 'returned';