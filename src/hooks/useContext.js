import React from 'react'
import { Context } from '../Context'

export const useContext = () => {    
    const context = React.useContext(Context)
    return context
}