import React, { useEffect, useRef } from 'react'

export const useWOW = ({
    boxClass = 'wow',
    animateClass = 'animated',
    offset = 0,
    mobile = true,
    live = true
} = {}) => {
    let init = useRef(false)

    useEffect(() => {
        if (init.current)
            return

        window.wow = new window.WOW({
            boxClass,
            animateClass,
            offset,
            mobile,
            live
        })

        window.wow.init();

        init.current = true;
    }, [])

    return [window.wow]
}