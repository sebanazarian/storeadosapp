import { client } from './../client'

export const user = async () => {
    const response = await client.get(`/user`);
    return response.data.data;
}

export const update = async (params) => {
    const response = await client.put(`/user`, params);
    return response.data.data;
}

export const statistics = async () => {
    const response = await client.get(`/user/statistics`);
    return response.data.data;
}

export const unsubscriber = async (provider, token) => {
    const response = await client.post(`/user/notifications/${provider}/unsubscriber`);
    return (response.status === 200 /* Unsubscriber */)
}

export const subscriber = async (provider, token) => {
    const response = await client.post(`/user/notifications/${provider}/subscriber`, { token });
    return (response.status === 200 /* Subscriber */)
}