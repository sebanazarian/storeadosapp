import { client } from './../client'

export const register = async (user) => {
    const response = await client.post(`/register`, user);
    return (response.status === 201 /* Created */)
}