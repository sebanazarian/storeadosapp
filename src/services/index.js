
import * as UserService from './User'
import * as AuthService from './Auth'
import * as BankAccountService from './BankAccount'
import * as DeliveryMethodService from './DeliveryMethod'
import * as PaymentOrderService from './PaymentOrder'
import * as RegisterService from './Register'
import * as StoreService from './Store'
import * as ProductService from './Product'
import * as PurchaseOrderService from './PurchaseOrder'
import * as CategoryService from './Category'
import * as PaymentMethodService from './PaymentMethod'
import * as FacebookService from './Facebook'

export {
    UserService,
    AuthService,
    BankAccountService,
    DeliveryMethodService,
    PaymentOrderService,
    RegisterService,
    StoreService,
    ProductService,
    PurchaseOrderService,
    CategoryService,
    PaymentMethodService,
    FacebookService
}